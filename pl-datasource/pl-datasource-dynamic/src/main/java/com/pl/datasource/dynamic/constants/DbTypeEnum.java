package com.pl.datasource.dynamic.constants;

/**
 * @ClasssName DbTypeEnum
 * @Description 数据库类型
 * @Author Liuyh
 * @Date 2021/9/16
 * @Version V0.0.1
 */
public enum DbTypeEnum {
    mysql
}
