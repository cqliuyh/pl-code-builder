package com.pl.data.core.entity;

import lombok.Data;

import java.util.List;

/**
 * @ClasssName IdsDTO
 * @Description id集合DTO
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@Data
public class IdsDTO {
    /**
     * id集合
     */
    private List<String> ids;
}
