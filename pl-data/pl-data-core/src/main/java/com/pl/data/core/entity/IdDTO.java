package com.pl.data.core.entity;

import lombok.Data;

/**
 * @ClasssName IdDTO
 * @Description id DTO
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@Data
public class IdDTO {
    /**
     * d
     */
    private String id;
}
