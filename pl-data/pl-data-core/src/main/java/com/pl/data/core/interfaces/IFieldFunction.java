package com.pl.data.core.interfaces;

import java.io.Serializable;

/**
 * @ClasssName IFieldFunction
 * @Description
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@FunctionalInterface
public interface IFieldFunction<T> extends Serializable {

    void apply(T t);


}
