# 数据库

1.数据库在根目录的`db`目录下  
2.数据库IP地址要么配置host，要么在`application.yml`中写死  
3.端口、用户名、密码可以配置环境变量，要么也是在`application.yml`中写死

# 工程目录

`pl-code-builder`->`pl-apps`->`pl-app-code`->`PlCodeGeneraterApplication.java`

# 注意事项 

1.记得修改`application.yml`配置文件

# 接下来请看VCR

## 1.动态数据源配置
![添加数据源](./images/01.png)  

![数据源列表](./images/02.png)  

![数据源详情](./images/03.png)  

![代码生成](./images/04.png)  

![生成结果](images/05.png)  

![生成配置](images/06.png)  

![生成配置](images/07.png)  



