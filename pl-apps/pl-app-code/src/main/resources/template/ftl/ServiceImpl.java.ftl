<#assign superClass=""/>
<#assign superGeneric=""/>
<#assign implements=""/>
<#if model.extendsSupperClass>
    <#assign superClass= "extends " + model.superClassName/>
    <#assign superGeneric= vmTools.buildGeneric(model.superGenericList)/>
    <#assign implements= " implements " + vmTools.buildImplements(model.implementsList)/>
</#if>
package ${packageName}.service.impl;

import org.springframework.stereotype.Service;
<#if model.importPackages??>
    <#list model.importPackages as pkg>
import ${pkg};
    </#list>

</#if>
/**
 * @ClasssName ${model.firstUpperCaseName}ServiceImpl
 * @Description ${model.comment}ServiceImpl
 * @Author ${author}
 * @Date ${date}
 * @Version V0.0.1
 */
@Service
public class ${model.firstUpperCaseName}ServiceImpl ${superClass}${superGeneric}${implements} {
<#if model.wired>
    <#if model.wiredType == 'resource'>

    @Resource
    private ${model.firstUpperCaseName}Mapper ${model.firstLowerCaseName}Mapper;
    </#if>
    <#if model.wiredType == 'autoWired'>

    @Autowired
    private ${model.firstUpperCaseName}Mapper ${model.firstLowerCaseName}Mapper;
    </#if>
    <#if model.wiredType == 'constructor'>

    private final ${model.firstUpperCaseName}Mapper ${model.firstLowerCaseName};

    public ${model.firstUpperCaseName}ServiceImpl(${model.firstUpperCaseName}Mapper ${model.firstLowerCaseName}Mapper) {
        this.${model.firstLowerCaseName}Mapper = ${model.firstLowerCaseName}Mapper;
    }
    </#if>
</#if>

}
