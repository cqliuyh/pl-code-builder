<#assign superClass=""/>
<#assign superGeneric=""/>
<#if model.extendsSupperClass>
    <#assign superClass= "extends " + model.superClassName/>
    <#assign superGeneric= vmTools.buildGeneric(model.superGenericList)/>
</#if>
package ${packageName}.service;

<#if model.importPackages??>
    <#list model.importPackages as pkg>
import ${pkg};
    </#list>

</#if>
/**
 * @ClasssName ${model.firstUpperCaseName}Service
 * @Description ${model.comment}Service
 * @Author ${author}
 * @Date ${date}
 * @Version V0.0.1
 */
public interface ${model.firstUpperCaseName}Service ${superClass}${superGeneric} {
}
