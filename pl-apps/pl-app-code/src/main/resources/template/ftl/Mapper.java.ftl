<#assign superClass=""/>
<#assign superGeneric=""/>
<#if model.extendsSupperClass>
    <#assign superClass= "extends " + model.superClassName/>
    <#assign superGeneric= vmTools.buildGeneric(model.superGenericList)/>
</#if>
package ${packageName}.dao;

import com.baomidou.mybatisplus.annotation.TableName;
<#if model.importPackages??>
    <#list model.importPackages as pkg>
import ${pkg};
    </#list>

</#if>
/**
 * @ClasssName ${model.firstUpperCaseName}Mapper
 * @Description ${model.comment}Mapper
 * @Author ${author}
 * @Date ${date}
 * @Version V0.0.1
 */
<#if model.annotations??>

    <#list model.annotations as annotation>
${annotation.name}${vmTools.buildAnnotayionProperties(annotation.properties)}
    </#list>
</#if>
@TableName("${model.originalName}")
public interface ${model.firstUpperCaseName}Mapper ${superClass}${superGeneric} {
}
