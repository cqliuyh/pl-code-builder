<#assign pagePkg=""/>
<#assign pageClsName=""/>
<#if style == 'pl-web'>
    <#assign pageClsName= "AbstractPage"/>
    <#assign pagePkg= "import com.pl.data.core.entity.dto.AbstractPage;"/>
<#elseif style == 'mybatis-plus'>
    <#assign pageClsName= "Page"/>
    <#assign pagePkg= "import com.baomidou.mybatisplus.extension.plugins.pagination.Page;"/>
</#if>
package ${packageName}.controller;

<#if model.wired>
import ${packageName}.service.${model.firstUpperCaseName}Service;
</#if>
<#if swagger>
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
</#if>
<#if model.basicApi>
import ${model.responsePackageName};
${pagePkg}
</#if>
<#if model.importPackages??>
    <#list model.importPackages as pkg>
import ${pkg};
    </#list>

</#if>
/**
* @ClasssName ${model.firstUpperCaseName}Controller
* @Description ${model.comment}Controller
* @Author ${author}
* @Date ${date}
* @Version V0.0.1
*/
<#if model.annotations??>

    <#list model.annotations as annotation>
${annotation.name}${vmTools.buildAnnotayionProperties(annotation.properties)}
    </#list>
</#if>
@Api(value = "${path}", tags = "${model.comment}API接口")
public class ${model.firstUpperCaseName}Controller {
<#if model.wired>
    <#if model.wiredType == 'resource'>

    @Resource
    private ${model.firstUpperCaseName}Service ${model.firstLowerCaseName}Service;
    </#if>
    <#if model.wiredType == 'autoWired'>

    @AutoWired
    private ${model.firstUpperCaseName}Service ${model.firstLowerCaseName}Service;
    </#if>
    <#if model.wiredType == 'constructor'>

    private final ${model.firstUpperCaseName}Service ${model.firstLowerCaseName}Service;

    public ${model.firstUpperCaseName}Controller(${model.firstUpperCaseName}Service ${model.firstLowerCaseName}Service) {
        this.${model.firstLowerCaseName}Service = ${model.firstLowerCaseName}Service;
    }
    </#if>
    <#if model.basicApi && style != 'mybatis'>

    /**
     * 列表查询
     * @param page 列表查询
     * @param ${model.firstLowerCaseName} ${model.comment}
     * @return
     */
        <#if swagger>
    @ApiOperation(value = "列表查询", notes = "列表查询")
        </#if>
    @GetMapping("/list" )
    public ${model.responseClassName} get${model.firstUpperCaseName}List(${model.firstUpperCaseName} ${model.firstLowerCaseName}) {
        return ${model.responseClassName}.${model.successMethod}(${model.firstLowerCaseName}Service.list(Wrappers.query(${model.firstLowerCaseName})));
    }

    /**
     * 分页查询
     * @param page 分页对象
     * @param ${model.firstLowerCaseName} ${model.comment}
     * @return
     */
    <#if swagger>
    @ApiOperation(value = "分页查询", notes = "分页查询")
    </#if>
    @GetMapping("/page" )
    public ${model.responseClassName} get${model.firstUpperCaseName}Page(${pageClsName} page, ${model.firstUpperCaseName} ${model.firstLowerCaseName}) {
        return ${model.responseClassName}.${model.successMethod}(${model.firstLowerCaseName}Service.page(page, Wrappers.query(${model.firstLowerCaseName})));
    }

    /**
     * 新增
     *
     * @param ${model.firstLowerCaseName} ${model.comment}
     * @return
     */
    <#if swagger>
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "PUT")
    </#if>
    @PutMapping("add")
    public ${model.responseClassName} add(${model.firstUpperCaseName} ${model.firstLowerCaseName}) {
        return ${model.responseClassName}.${model.successMethod}(${model.firstLowerCaseName}Service.save(${model.firstLowerCaseName}));
    }

    /**
     * 删除${model.comment}
     *
     * @param ${model.pkName} id
     * @return
     */
    <#if swagger>
    @ApiOperation(value = "通过id删除${model.comment}", notes = "通过id删除${model.comment}", httpMethod = "DELETE")
    </#if>
    @DeleteMapping("/{${model.pkName}}")
    public ${model.responseClassName} delete(@PathVariable("${model.pkName}") ${model.pkJavaType} ${model.pkName}) {
        return ${model.responseClassName}.${model.successMethod}(${model.firstLowerCaseName}Service.removeById(id));
    }

    /**
     * 修改${model.comment}
     *
     * @param ${model.firstLowerCaseName} ${model.comment}
     * @return
     */
    <#if swagger>
    @ApiOperation(value = "修改${model.comment}", notes = "修改${model.comment}", httpMethod = "POST")
    </#if>
    @PostMapping("update")
    public ${model.responseClassName} update(${model.firstUpperCaseName} ${model.firstLowerCaseName}) {
        return  ${model.responseClassName}.${model.successMethod}(${model.firstLowerCaseName}Service.updateById(${model.firstLowerCaseName}));
    }

    /**
     * 通过id查询${model.comment}
     * @param ${model.pkName} id
     * @return
     */
    <#if swagger>
    @ApiOperation(value = "通过id查询${model.comment}", notes = "通过id查询${model.comment}", httpMethod = "GET")
    </#if>
    @GetMapping("/{${model.pkName}")
    public ${model.responseClassName} getById(@PathVariable("${model.pkName}") ${model.pkJavaType} ${model.pkName}) {
        return ${model.responseClassName}.${model.successMethod}(${model.firstLowerCaseName}Service.getById(id));
    }

    </#if>
</#if>

}
