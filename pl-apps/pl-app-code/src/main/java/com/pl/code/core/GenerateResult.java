package com.pl.code.core;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClasssName GenerateResultDTO
 * @Description 代码生成结果集
 * @Author Liuyh
 * @Date 2021/5/18
 * @Version V0.0.1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenerateResult {
    /**
     * Entity代码
     */
    @JSONField(name = "Entity")
    private String entity;

    /**
     * mapper代码
     */
    @JSONField(name = "Mapper")
    private String mapper;

    /**
     * mapperxml代码
     */
    @JSONField(name = "MapperXML")
    private String mapperXml;

    /**
     * service接口代码
     */
    @JSONField(name = "Service")
    private String service;

    /**
     * service实现类代码
     */
    @JSONField(name = "ServiceImpl")
    private String serviceImpl;

    /**
     * controller代码
     */
    @JSONField(name = "Controller")
    private String controller;
}
