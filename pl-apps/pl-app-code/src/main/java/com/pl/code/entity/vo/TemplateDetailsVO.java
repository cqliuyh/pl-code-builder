package com.pl.code.entity.vo;

import com.pl.code.entity.po.PlTemplate;
import lombok.Data;
import org.springframework.beans.BeanUtils;


/**
 * @ClasssName TemplateDetailsVO
 * @Description 模板详情VO
 * @Author Liuyh
 * @Date 2021/5/15
 * @Version V0.0.1
 */
@Data
public class TemplateDetailsVO {
    // 主键/配置id
    private Long id;

    // 是否为默认配置
    private Boolean defaultFlag;

    // 配置名称
    private String name;

    // 实体代码模板
    private String entity;

    // mapper代码模板
    private String mapper;

    // mapperXml代码模板
    private String mapperXml;

    // service代码模板
    private String service;

    // serviceImpl代码模板
    private String serviceImpl;

    // controller代码模板
    private String controller;

    /**
     * po转换成vo
     * @param po
     * @return
     */
    public static TemplateDetailsVO po2vo(PlTemplate po){
        TemplateDetailsVO vo = new TemplateDetailsVO();
        BeanUtils.copyProperties(po,vo);
        return vo;
    }
}
