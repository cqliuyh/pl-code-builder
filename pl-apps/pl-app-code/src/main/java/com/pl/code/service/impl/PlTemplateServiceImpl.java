package com.pl.code.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pl.code.entity.po.PlTemplate;
import com.pl.code.mapper.PlTemplateMapper;
import com.pl.code.service.PlTemplateService;
import org.springframework.stereotype.Service;

@Service
public class PlTemplateServiceImpl extends ServiceImpl<PlTemplateMapper, PlTemplate> implements PlTemplateService {
}
