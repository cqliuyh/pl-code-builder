package com.pl.code.config.properties;

import lombok.Data;

/**
 * @ClasssName ControllerConfigProperties
 * @Description controller 配置属性
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@Data
public class ControllerConfigProperties {
    // 是否生成基础API接口，如：page、list、getById、remove、save、update
    private boolean basicApi;
    // 响应实体配置属性
    private ResponseEntityConfigProperties responseEntity;
}
