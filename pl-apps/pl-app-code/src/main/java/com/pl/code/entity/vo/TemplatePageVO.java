package com.pl.code.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClasssName TemplatePageVO
 * @Description 模板分页VO
 * @Author Liuyh
 * @Date 2021/5/15
 * @Version V0.0.1
 */
@Data
public class TemplatePageVO {
    private Long id;
    private Boolean defaultFlag;
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
