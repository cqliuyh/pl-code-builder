package com.pl.code.entity.dto.template;

import com.pl.code.entity.po.PlTemplate;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotBlank;

/**
 * @ClasssName AbstractTemplateSaveDTO
 * @Description 模板配置参数公共抽象类
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@Data
public class AbstractTemplateSaveDTO {
    // 配置名称
    @NotBlank(message = "配置名称不能为空")
    private String name;

    // 实体代码模板
    private String entity;

    // mapper代码模板
    private String mapper;

    // mapperXml代码模板
    private String mapperXml;

    // service代码模板
    private String service;

    // serviceImpl代码模板
    private String serviceImpl;

    // controller代码模板
    private String controller;

    public PlTemplate dto2po() {
        PlTemplate po = new PlTemplate();
        BeanUtils.copyProperties(this, po);
        return po;
    }
}
