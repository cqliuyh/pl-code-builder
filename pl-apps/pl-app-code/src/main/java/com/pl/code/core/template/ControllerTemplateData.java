package com.pl.code.core.template;

import lombok.Data;

/**
 * @ClasssName ControllerTemplateData
 * @Description  Controller模板数据
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@Data
public class ControllerTemplateData extends ClassTemplateData{
    // 是否生成基础API接口
    private boolean basicApi;
    // 响应实体包名
    private String responsePackageName;
    // 响应实体类名
    private String responseClassName;
    // 成功方法名
    private String successMethod;
    // 失败方法名
    private String errorMethod;
    // 主键Java类型
    private String pkJavaType;
    // 主键字段名称
    private String pkName;
}
