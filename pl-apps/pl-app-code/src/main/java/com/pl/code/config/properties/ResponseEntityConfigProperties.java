package com.pl.code.config.properties;

import lombok.Data;

/**
 * @ClasssName ResponseEntityConfigProperties
 * @Description 响应实体配置属性
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@Data
public class ResponseEntityConfigProperties {
    // 包名
    private String packageName;
    // 成功方法名
    private String successMethod;
    // 失败/错误方法名
    private String errorMethod;
}
