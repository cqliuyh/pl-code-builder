package com.pl.code.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pl.code.entity.vo.TemplatePageVO;
import com.pl.data.core.entity.AbstractPlEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * @ClasssName PlTemplate
 * @Description 模板配置
 * @Author Liuyh
 * @Date 2021/5/15
 * @Version V0.0.1
 */
@Data
@TableName("pl_template")
public class PlTemplate extends AbstractPlEntity<PlGenerateConfig> {
    // 主键/配置id
    @TableId(type = IdType.AUTO, value = "id")
    private Long id;

    // 是否为默认配置
    private Boolean defaultFlag;

    // 配置名称
    private String name;

    // 实体代码模板
    private String entity;

    // mapper代码模板
    private String mapper;

    // mapperXml代码模板
    private String mapperXml;

    // service代码模板
    private String service;

    // serviceImpl代码模板
    private String serviceImpl;

    // controller代码模板
    private String controller;


    public TemplatePageVO po2vo(){
        TemplatePageVO vo = new TemplatePageVO();
        BeanUtils.copyProperties(this,vo);
        return vo;
    }


}
