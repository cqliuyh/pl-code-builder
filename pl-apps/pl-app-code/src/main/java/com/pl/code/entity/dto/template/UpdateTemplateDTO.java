package com.pl.code.entity.dto.template;

import com.pl.code.entity.po.PlTemplate;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateTemplateDTO extends AbstractTemplateSaveDTO{

    @NotNull(message = "id不能为空")
    private Long id;

    @Override
    public PlTemplate dto2po() {
        PlTemplate po = super.dto2po();
        po.setId(this.id);
        return po;
    }
}
