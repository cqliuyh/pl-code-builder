package com.pl.code.config.properties;

import lombok.Data;

import java.util.List;

/**
 * @ClasssName MapperConfigProperties
 * @Description Mapper配置属性
 * @Author Liuyh
 * @Date 2021/5/20
 * @Version V0.0.1
 */
@Data
public class MapperConfigProperties {
    // 主键类型
    private String idType;
    // 是否生成xml
    private boolean buildXml;
    // 是否添加@Mapper注解
    private boolean mapperAnnotation;
    // 要生成的方法
    private List<String> method;
}
