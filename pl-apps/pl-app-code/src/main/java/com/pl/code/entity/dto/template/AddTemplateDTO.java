package com.pl.code.entity.dto.template;

import com.pl.code.entity.po.PlTemplate;
import lombok.Data;

/**
 * @ClasssName AddTemplateDTO
 * @Description 新增模板参数
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@Data
public class AddTemplateDTO extends AbstractTemplateSaveDTO{

}
