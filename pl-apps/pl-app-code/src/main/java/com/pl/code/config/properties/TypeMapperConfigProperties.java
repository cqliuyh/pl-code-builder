package com.pl.code.config.properties;

import lombok.Data;

/**
 * @ClasssName TypeMapperConfigProperties
 * @Description 数据类型映射配置
 * @Author Liuyh
 * @Date 2021/5/25
 * @Version V0.0.1
 */
@Data
public class TypeMapperConfigProperties {
    // 数据库数据类型
    private String columnType;
    // java数据类型
    private String javaType;
    // 是否需要导包
    private boolean importPackage;
    // 包名
    private String packageName;
}
