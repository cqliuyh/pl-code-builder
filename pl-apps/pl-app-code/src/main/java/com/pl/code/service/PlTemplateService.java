package com.pl.code.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pl.code.entity.po.PlTemplate;

public interface PlTemplateService extends IService<PlTemplate> {
}
