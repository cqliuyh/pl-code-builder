package com.pl.code.config.properties;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @ClasssName LombokConfigProperties
 * @Description lombok配置属性
 * @Author Liuyh
 * @Date 2021/5/20
 * @Version V0.0.1
 */
@Data
public class LombokConfigProperties {
    // 是否开启lombok
    private Boolean enable;
    // @builder注解
    private Boolean builder;
    // @data注解
    private Boolean data;
    // @getter注解
    private Boolean getter;
    // @setter注解
    private Boolean setter;
    // @ToString注解
    private Boolean toStr;
    // @equalsAndHashCode注解
    private Boolean equalsAndHashCode;
    // @allArgsConstructor注解
    private Boolean allArgsConstructor;
    // @noAllArgsConstructor 注解
    private Boolean noAllArgsConstructor;
    // @accessors 注解配置
    private Accessors accessors;


    @Data
    public static class Accessors {
        // 是否开启
        private Boolean enable;
        // 模式
        private String mode;
        // 模式属性
        private String prefixModeValue;
    }

    /**
     * 是否开启lombok注解
     * @return
     */
    public boolean hasAnnotation() {
        return enable && (builder || data || getter || setter || toStr || equalsAndHashCode || allArgsConstructor || noAllArgsConstructor || isEnableAccessors());
    }

    /**
     * 是否开启@accessors注解
     * @return
     */
    public boolean isEnableAccessors() {
        return accessors != null && accessors.enable;
    }

    /**
     * 获取@accessors注解的属性
     * @return
     */
    public String getAccessorsPropertity() {
        if (StringUtils.isBlank(accessors.mode)) {
            return null;
        }
        if ("prefix".equalsIgnoreCase(accessors.mode)) {
            if (StringUtils.isBlank(accessors.prefixModeValue)){
                return null;
            } else {
                return "prefix=" + accessors.prefixModeValue;
            }

        }
        return accessors.mode + "=true";

    }
}
