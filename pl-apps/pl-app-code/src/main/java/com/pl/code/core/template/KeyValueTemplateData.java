package com.pl.code.core.template;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClasssName KeyValueTemplateData
 * @Description 键值对模板数据
 * @Author Liuyh
 * @Date 2021/5/25
 * @Version V0.0.1
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyValueTemplateData {
    // 键
    private String key;
    // 值
    private String value;
}
