package com.pl.code.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pl.code.entity.po.PlTemplate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlTemplateMapper extends BaseMapper<PlTemplate> {
}
