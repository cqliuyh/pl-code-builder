package com.pl.code.entity.po;

import lombok.Data;

/**
 * @ClasssName TableInfo
 * @Description 表信息
 * @Author Liuyh
 * @Date 2021/5/17
 * @Version V0.0.1
 */
@Data
public class TableInfo {
    // 表名
    private String tableName;
    // 表注释
    private String tableComment;
    // 创建时间
    private String createTime;
}
