package com.pl.code.desktop.core.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.pl.code.desktop.core.config.properties.DataTypeProperties;
import com.pl.code.desktop.entity.bo.datasource.TableColumnBO;
import com.pl.core.exception.BusinessException;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClasssName GeneratorUtil
 * @Description 代码生成工具类
 * @Author Liuyh
 * @Date 2021/5/29
 * @Version V0.0.1
 */
@UtilityClass
public class GeneratorUtil {

    /**
     * 添加包列表
     *
     * @param packages
     * @param packageName
     */
    public void putPackage(List<String> packages, String packageName) {
        if (StringUtils.isBlank(packageName)) {
            return;
        }
        if (packages == null) {
            packages = new ArrayList<>();
        }
        if (!packages.contains(packageName)) {
            packages.add(packageName);
        }
    }


    /**
     * 判断列名是否存在
     *
     * @param columns
     * @param column
     * @return
     */
    public boolean columnExist(List<String> columns, String column) {
        if (CollectionUtil.isEmpty(columns)) {
            return false;
        }
        List<String> columnList = new ArrayList<>();
        columns.forEach(item -> {
            columnList.add(item.toLowerCase());
        });
        return columnList.contains(column.toLowerCase());
    }

    /*
     */

    /**
     * 数据库类型转Java类型
     *
     * @param dataTypePropertiesList
     * @param type
     * @return
     */
    public DataTypeProperties colType2JavaType(List<DataTypeProperties> dataTypePropertiesList, String type) {
        DataTypeProperties dataType = null;
        if (StrUtil.isNotBlank(type) && CollectionUtil.isNotEmpty(dataTypePropertiesList)) {
            dataType = dataTypePropertiesList.stream().filter(item -> item.getColumnType().equalsIgnoreCase(type)).findFirst().orElse(null);
        }
        if (dataType == null) {
            dataType = new DataTypeProperties();

            dataType.setJavaType("Object");
        }
        return dataType;
    }


    /**
     * 表名转换成Java类名
     */
    public String tableToClsName(String tableName, String tablePrefix) {
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replaceFirst(tablePrefix, "");
        }
        return col2java(tableName);
    }


    /**
     * 下划线转驼峰
     */
    public String col2java(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_", "");
    }

    /**
     * 首字母小写
     *
     * @param text
     * @return
     */
    public String firstLowerCase(String text) {
        return StringUtils.uncapitalize(text);
    }

    /**
     * 首字母大写
     *
     * @param text
     * @return
     */
    public String firstUpperCase(String text) {
        return StringUtils.capitalize(text);
    }

    /**
     * 首字母小写并驼峰
     *
     * @param text
     * @return
     */
    public String firstLowerCaseAndHump(String text) {
        if (text.contains("_")) {
            text = col2java(text);
        }
        return StringUtils.uncapitalize(text);
    }

    /**
     * 首字母大写并驼峰
     *
     * @param text
     * @return
     */
    public String firstUpperCaseAndHump(String text) {
        if (text.contains("_")) {
            text = col2java(text);
        }
        return StringUtils.capitalize(text);
    }

    /**
     * 获取主键
     *
     * @param columnList
     * @return
     */
    public TableColumnBO getPk(List<TableColumnBO> columnList) {
        for (TableColumnBO column : columnList) {
            // 是否为主键
            boolean isPk = "pri".equalsIgnoreCase(column.getColumnKey());
            if (isPk) {
                return column;
            }
        }
        return null;
    }

    /**
     * 包名转类名
     * @param packageName
     * @return
     */
    public String packageToClassName(String packageName){
       return packageName.substring(packageName.lastIndexOf(".") + 1);
    }

    public static void main(String[] args) {
        System.err.println(col2java("BaseMapper"));
        System.err.println(firstUpperCaseAndHump("BaseMapper"));
    }
}
