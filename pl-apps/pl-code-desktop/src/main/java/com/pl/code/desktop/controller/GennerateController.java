package com.pl.code.desktop.controller;

import com.pl.code.desktop.core.web.AbstractDataSourceController;
import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import com.pl.data.core.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClasssName GennerateController
 * @Description 生成管理
 * @Author Liuyh
 * @Date 2021/9/29
 * @Version V0.0.1
 */
@RestController
@RequestMapping("gennerate")
@Api(tags = "生成管理")
public class GennerateController extends AbstractDataSourceController {
/*
    @ApiOperation("表详情")
    @GetMapping("{id}/{tableName}")
    public Result<ClassDataModel> getTableDetails(
            @PathVariable("id") @ApiParam(value = "数据源id", required = true) Long id,
            @PathVariable("tableName") @ApiParam(value = "表名", required = true) String tableName,
            @ApiParam(value = "配置id", required = true) Long configId) {
        PlDataSource dataSource = getDataSourceById(id);
        ClassDataModel dataModel = EEEEntityDataModelBuilder.create().table(tableName).datasource(dataSource.getName()).configId(configId).customConfig(null).builder();
        return Result.ok(dataModel);
    }*/
}
