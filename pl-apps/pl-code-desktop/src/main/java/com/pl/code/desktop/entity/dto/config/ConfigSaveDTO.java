package com.pl.code.desktop.entity.dto.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ClasssName ConfigSaveDTO
 * @Description 配置项保存DTO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("配置项保存DTO")
@Data
public class ConfigSaveDTO {

    @NotBlank(message = "配置名称不能为空")
    @ApiModelProperty("配置名称")
    private String name;

    @NotBlank(message = "配置内容不能为空")
    @ApiModelProperty("配置内容")
    private String content;
}
