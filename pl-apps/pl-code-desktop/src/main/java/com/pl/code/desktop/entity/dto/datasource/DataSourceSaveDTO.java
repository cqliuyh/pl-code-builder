package com.pl.code.desktop.entity.dto.datasource;

import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import com.pl.core.utils.AssertUtil;
import com.pl.datasource.dynamic.constants.ConnectionTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotBlank;

/**
 * @ClasssName DataSourceSaveDTO
 * @Description 数据源新增/修改DTO
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@Data
public class DataSourceSaveDTO {

    /**
     * 名称
     */
    @NotBlank(message = "数据源名称不能为空")
    @ApiModelProperty("名称")
    private String name;

    /**
     * 连接类型 0:主机连接,1:URl连接
     */
    @NotBlank(message = "连接类型不能为空")
    @ApiModelProperty(value = "连接类型 0:主机连接,1:URl连接",required = true)
    private Integer connectionType;

    /**
     * 数据库类型
     */
    @NotBlank(message = "数据库类型不能为空")
    @ApiModelProperty("数据库类型")
    private String dbType;

    /**
     * 主机
     */
    @ApiModelProperty("主机")
    private String host;

    /**
     * 端口
     */
    @ApiModelProperty("端口")
    private String port;

    /**
     * 实例名
     */
    @ApiModelProperty("实例名")
    private String instance;

    /**
     * 数据库名称
     */
    @ApiModelProperty("数据库名称")
    private String dbName;

    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String url;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    @ApiModelProperty("密码")
    private String password;

    /**
     * 验证表单
     */
    public void validFrom() {
        if (ConnectionTypeEnum.URL.typeEquals(connectionType)) {
            AssertUtil.notBlank(url, "URL不能为空");
        } else {
            AssertUtil.notBlank(host, "主机ip不能为空");
            AssertUtil.notBlank(port, "端口不能为空");
            AssertUtil.notBlank(dbName, "数据库不能为空");
        }
    }

    public PlDataSource toPlDataSource() {
        PlDataSource dataSource = new PlDataSource();
        BeanUtils.copyProperties(this, dataSource);
        return dataSource;
    }
}
