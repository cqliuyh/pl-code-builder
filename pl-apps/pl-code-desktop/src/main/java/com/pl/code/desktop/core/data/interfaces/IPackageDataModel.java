package com.pl.code.desktop.core.data.interfaces;

import java.util.List;

/**
 * @ClasssName IPackageDataModel
 * @Description 包名接口
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
public interface IPackageDataModel {
    /**
     * 获取包名
     * @return
     */
    public List<String> getPackageList();
}
