package com.pl.code.desktop.service.datasource;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pl.code.desktop.entity.dto.datasource.DataSourceListDTO;
import com.pl.code.desktop.entity.dto.datasource.DataSourcePageDTO;
import com.pl.code.desktop.entity.dto.datasource.DataSourceSaveDTO;
import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import com.pl.code.desktop.entity.vo.datasource.DataSourceListVO;
import com.pl.code.desktop.entity.vo.datasource.DataSourcePageVO;
import com.pl.data.core.entity.PageVO;

import java.util.List;

/**
 * @ClasssName IDataSourceService
 * @Description 数据源接口
 * @Author Liuyh
 * @Date 2021/9/16
 * @Version V0.0.1
 */
public interface IDataSourceService extends IService<PlDataSource> {
    /**
     * 添加数据源
     * @param dataSource
     * @return
     */
    boolean add(DataSourceSaveDTO dataSource);

    /**
     * 删除数据源
     * @param id
     * @return
     */
    boolean remove(Long id);

    /**
     * 修改数据源
     * @param id
     * @param updateDTO
     * @return
     */
    boolean update(Long id, DataSourceSaveDTO updateDTO);

    /**
     * 列表
     * @param listDTO
     * @return
     */
    List<DataSourceListVO> list(DataSourceListDTO listDTO);

    /**
     * 分页列表
     * @param pageDTO
     * @return
     */
    PageVO<DataSourcePageVO> page(DataSourcePageDTO pageDTO);

    /**
     * 通过数据源名称获取数据源
     * @return
     */
    PlDataSource getByDsName(String name);



}
