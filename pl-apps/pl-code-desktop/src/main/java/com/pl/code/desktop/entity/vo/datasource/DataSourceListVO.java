package com.pl.code.desktop.entity.vo.datasource;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName DataSourceListVO
 * @Description 数据源列表VO
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@Data
public class DataSourceListVO {
    @ApiModelProperty("数据源id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 连接类型 0:主机连接,1:URl连接
     */
    @ApiModelProperty("连接类型 0:主机连接,1:URl连接")
    private Integer connectionType;

    /**
     * 数据库类型
     */
    @ApiModelProperty("数据库类型")
    private String dbType;

    /**
     * 主机
     */
    @ApiModelProperty("主机")
    private String host;

    /**
     * 端口
     */
    @ApiModelProperty("端口")
    private String port;

    /**
     * 实例名
     */
    @ApiModelProperty("实例名")
    private String instance;

    /**
     * 数据库名称
     */
    @ApiModelProperty("数据库名称")
    private String dbName;

    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String url;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;
}
