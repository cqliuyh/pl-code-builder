package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @ClasssName SwaggerProperties
 * @Description Swagger配置属性
 * @Author Liuyh
 * @Date 2021/9/26
 * @Version V0.0.1
 */
@ApiModel("Swagger配置属性")
@Data
public class SwaggerProperties {
    // 是否开启swagger
    @ApiModelProperty("是否开启swagger")
    private boolean enable;
    // 模式 simple:简单模式，full:完整模式
    @ApiModelProperty("模式 simple:简单模式，full:完整模式")
    private String model;
    // @ApiModel注解
    @ApiModelProperty("@ApiModel注解")
    private boolean apiModel;
    // @ApiOperation
    @ApiModelProperty("@ApiOperation注解")
    private boolean apiOperation;
}
