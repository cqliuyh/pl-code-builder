package com.pl.code.desktop.entity.vo.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName ConfigDetailsVO
 * @Description 配置详情VO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("配置详情VO")
@Data
public class ConfigDetailsVO {

    @ApiModelProperty("配置id")
    private Long id;

    @ApiModelProperty("配置名称")
    private String name;

    @ApiModelProperty("配置内容")
    private String content;
}
