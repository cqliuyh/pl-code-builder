package com.pl.code.desktop.core.data.interfaces;

import com.pl.code.desktop.core.data.model.factory.ExtendsFactory;

/**
 * @ClasssName IExtendsFactory
 * @Description 继承工厂接口
 * @Author Liuyh
 * @Date 2021/10/21
 * @Version V0.0.1
 */
public interface IExtendsFactory {
    /**
     * 继承
     * @param factory
     */
    void extend(ExtendsFactory factory);
}
