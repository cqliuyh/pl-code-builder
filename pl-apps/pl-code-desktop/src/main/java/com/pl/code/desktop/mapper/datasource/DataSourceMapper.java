package com.pl.code.desktop.mapper.datasource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClasssName DataSourceMapper
 * @Description 数据源mapper
 * @Author Liuyh
 * @Date 2021/5/12
 * @Version V0.0.1
 */
@Mapper
public interface DataSourceMapper extends BaseMapper<PlDataSource> {
}
