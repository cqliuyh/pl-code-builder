package com.pl.code.desktop.entity.bo.datasource;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClasssName TableInfoBO
 * @Description 表信息BO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("表信息BO")
@Data
public class TableInfoBO {

    @ApiModelProperty("数据源名称")
    private String dsName;

    @ApiModelProperty("数据库名称")
    private String dbName;

    @ApiModelProperty("表名")
    private String tableName;

    @ApiModelProperty("注释")
    private String tableComment;

    @ApiModelProperty("数据库类型")
    private String tableType;

    @ApiModelProperty("引擎")
    private String engine;

    @ApiModelProperty("行数")
    private long rows;

    @ApiModelProperty("数据长度")
    private Long dataLength;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @ApiModelProperty("排序规则")
    private String tableCollation;
}
