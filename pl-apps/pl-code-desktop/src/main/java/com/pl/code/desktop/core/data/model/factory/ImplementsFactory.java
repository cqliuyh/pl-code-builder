package com.pl.code.desktop.core.data.model.factory;

import com.pl.code.desktop.core.data.model.cls.ClassDataModel;
import com.pl.code.desktop.core.data.model.cls.ExtendsClassDataModel;
import com.pl.code.desktop.core.data.model.cls.GenericDataModel;
import com.pl.code.desktop.core.data.model.cls.ImplementsDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;

import java.util.List;

/**
 * @ClasssName ImplementsFactory
 * @Description 实现工厂
 * @Author Liuyh
 * @Date 2021/10/21
 * @Version V0.0.1
 */
public class ImplementsFactory {

    private ClassDataModel classDataModel;

    public ImplementsFactory(ClassDataModel classDataModel) {
        this.classDataModel = classDataModel;
    }



    /**
     * 创建实现接口工厂
     *
     * @param packageName
     * @return
     */
    public ImplementsFactory create(String packageName) {
        String name = DataModelUtil.getClassNameByPackageName(packageName);
        return create(packageName, name);
    }

    /**
     * 创建实现接口工厂
     *
     * @param packageName
     * @param name
     * @return
     */
    public ImplementsFactory create(String packageName, String name) {
        ImplementsDataModel dataModel = ImplementsDataModel.build(packageName, name);
        classDataModel.getImplementsList().add(dataModel);
        return this;
    }


    /**
     * 添加泛型
     *
     * @param packageName
     * @param className
     * @return
     */
    public ImplementsFactory generic(String packageName, String className) {
        GenericDataModel genericDataModel = new GenericDataModel();
        genericDataModel.setPackageName(packageName);
        genericDataModel.setClassName(className);
        List<ImplementsDataModel> implementsList = classDataModel.getImplementsList();
        implementsList.get(implementsList.size() - 1).generic(packageName, className);
        return this;
    }

    /**
     * 添加泛型(当前类)
     *
     * @param className
     * @return
     */
    public ImplementsFactory genericThis(String className) {
        return generic(null, className);
    }

    /**
     * 添加泛型(当前类)
     * @return
     */
    public ImplementsFactory genericThis() {
        return generic(null, classDataModel.getClassName());
    }


    public ImplementsFactory and(){
        return new ImplementsFactory(classDataModel);
    }
}
