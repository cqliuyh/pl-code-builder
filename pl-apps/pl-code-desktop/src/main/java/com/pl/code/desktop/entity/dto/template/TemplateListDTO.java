package com.pl.code.desktop.entity.dto.template;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName TemplateListDTO
 * @Description 模板列表DTO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("模板列表DTO")
@Data
public class TemplateListDTO {

    @ApiModelProperty("模板名称")
    private String name;

}