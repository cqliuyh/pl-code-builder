package com.pl.code.desktop.controller;

import com.pl.code.desktop.entity.dto.datasource.DataSourceListDTO;
import com.pl.code.desktop.entity.dto.datasource.DataSourcePageDTO;
import com.pl.code.desktop.entity.dto.datasource.DataSourceSaveDTO;
import com.pl.code.desktop.entity.vo.datasource.DataSourceListVO;
import com.pl.code.desktop.entity.vo.datasource.DataSourcePageVO;
import com.pl.code.desktop.service.datasource.IDataSourceService;
import com.pl.data.core.entity.PageVO;
import com.pl.data.core.response.Result;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClasssName DataSourceController
 * @Description 数据源相关API
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@RestController
@RequestMapping("datasource")
@Api(tags = "数据源管理")
public class DataSourceController {
    @Autowired
    private IDataSourceService dataSourceService;

    /**
     * 新增数据源
     *
     * @param addDTO
     * @return
     */
    @ApiOperation("新增数据源")
    @PostMapping("add")
    public Result<Boolean> add(@Validated DataSourceSaveDTO addDTO) {
        return Result.ok(dataSourceService.add(addDTO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiOperation("删除数据源")
    @DeleteMapping("{id}")
    public Result<Boolean> remove(@PathVariable("id") @ApiParam(value = "数据源id", required = true) Long id) {
        return Result.ok(dataSourceService.remove(id));
    }

    /**
     * 更新
     *
     * @param id
     * @param updateDTO
     * @return
     */
    @ApiOperation("更新数据源")
    @PostMapping("{id}")
    public Result<Boolean> update(@PathVariable("id") @ApiParam(value = "数据源id", required = true) Long id,
                                  @Validated DataSourceSaveDTO updateDTO) {
        return Result.ok(dataSourceService.update(id, updateDTO));
    }

    /**
     * 列表
     *
     * @param listDTO
     * @return
     */
    @ApiOperation("数据源列表")
    @GetMapping("list")
    public Result<List<DataSourceListVO>> list(DataSourceListDTO listDTO) {
        return Result.ok(dataSourceService.list(listDTO));
    }

    /**
     * 分页列表
     *
     * @param pageDTO
     * @return
     */
    @ApiOperation("数据源分页列表")
    @GetMapping("page")
    public Result<PageVO<DataSourcePageVO>> page(DataSourcePageDTO pageDTO) {
        return Result.ok(dataSourceService.page(pageDTO));
    }


}
