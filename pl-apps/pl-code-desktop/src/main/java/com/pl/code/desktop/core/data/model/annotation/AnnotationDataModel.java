package com.pl.code.desktop.core.data.model.annotation;

import com.pl.code.desktop.core.data.interfaces.IPackageDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClasssName AnnotationDataModel
 * @Description 注解数据模型
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
@Data
public class AnnotationDataModel implements IPackageDataModel {
    // 包名
    private String packageName;
    // 注解名称
    private String name;
    // 属性列表
    private List<AnnotationPropertyDataModel> properties = new ArrayList<>();



    /**
     * 创建一个注解
     *
     * @param packageName
     * @return
     */
    public static AnnotationDataModel create(String packageName) {
        String name = DataModelUtil.getClassNameByPackageName(packageName);
        return create(packageName, name);
    }

    /**
     * 创建一个注解
     *
     * @param packageName
     * @param name
     * @return
     */
    public static AnnotationDataModel create(String packageName, String name) {
        AnnotationDataModel annotationDataModel = new AnnotationDataModel();
        annotationDataModel.setPackageName(packageName);
        annotationDataModel.setName(name);
        return annotationDataModel;
    }


    /**
     * 添加属性
     *
     * @param property
     * @return
     */
    public AnnotationDataModel property(AnnotationPropertyDataModel property) {
        properties.add(property);
        return this;
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @param packageName
     * @param quotation
     * @return
     */
    public AnnotationDataModel property(String key, String value, String packageName, boolean quotation) {
        AnnotationPropertyDataModel property = new AnnotationPropertyDataModel();
        property.setKey(key);
        property.setValue(value);
        property.setPackageName(packageName);
        property.setQuotation(quotation);
        properties.add(property);
        return this;
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @return
     */
    public AnnotationDataModel property(String key, String value) {
        return property(key, value, null);
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @return
     */
    public AnnotationDataModel property(String key, String value, String packageName) {
        return property(key, value, packageName, false);
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @return
     */
    public AnnotationDataModel quotationProperty(String key, String value) {
        AnnotationPropertyDataModel property = new AnnotationPropertyDataModel();
        property.setKey(key);
        property.setValue(value);
        property.setQuotation(true);
        properties.add(property);
        return this;
    }



    /**
     * 获取包名
     *
     * @return
     */
    @Override
    public List<String> getPackageList() {
        List<String> packageList = new ArrayList<>();
        DataModelUtil.pushPackage(packageList, packageName);
        DataModelUtil.pushPackage(packageList, properties, AnnotationPropertyDataModel::getPackageName);
        return packageList;
    }
}
