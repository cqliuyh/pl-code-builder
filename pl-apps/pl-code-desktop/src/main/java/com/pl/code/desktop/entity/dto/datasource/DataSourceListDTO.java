package com.pl.code.desktop.entity.dto.datasource;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName DataSourceListDTO
 * @Description 数据源列表DTO
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@Data
@ApiModel("数据源列表DTO")
public class DataSourceListDTO {
    // 数据源名称
    @ApiModelProperty("数据源名称")
    private String name;
}
