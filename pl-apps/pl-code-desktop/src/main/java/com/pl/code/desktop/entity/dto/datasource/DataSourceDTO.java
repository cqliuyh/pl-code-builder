package com.pl.code.desktop.entity.dto.datasource;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ClasssName DataSourceDTO
 * @Description
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Data
public class DataSourceDTO {
    @ApiModelProperty("数据源名称")
    @NotBlank(message = "数据源名称不能为空")
    private String dsName;
}
