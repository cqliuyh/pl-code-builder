package com.pl.code.desktop.core.data.model.cls;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.pl.code.desktop.core.data.interfaces.IPackageDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClasssName ExtendsClassDataModel
 * @Description 继承类数据模型
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
@Data
public class ExtendsClassDataModel implements IPackageDataModel {
    // 包名
    private String packageName;
    // 类名
    private String className;
    // 泛型列表
    private List<GenericDataModel> generics = new ArrayList<>();

    /**
     * 创建继承类数据模型
     *
     * @param packageName
     * @return
     */
    public static ExtendsClassDataModel build(String packageName) {
        // 通过包名过去类名
        String className = DataModelUtil.getClassNameByPackageName(packageName);
        return build(packageName, className);
    }

    /**
     * 创建继承类数据模型
     *
     * @param packageName
     * @param className
     * @return
     */
    public static ExtendsClassDataModel build(String packageName, String className) {
        ExtendsClassDataModel extendsClassDataModel = new ExtendsClassDataModel();
        extendsClassDataModel.setPackageName(packageName);
        extendsClassDataModel.setClassName(className);
        return extendsClassDataModel;
    }

    /**
     * 添加泛型
     *
     * @param packageName
     * @return
     */
    public ExtendsClassDataModel generic(String packageName) {
        String className = DataModelUtil.getClassNameByPackageName(packageName);
        return generic(packageName, className);
    }

    /**
     * 添加泛型
     *
     * @param packageName
     * @param className
     * @return
     */
    public ExtendsClassDataModel generic(String packageName, String className) {
        GenericDataModel genericDataModel = new GenericDataModel();
        genericDataModel.setPackageName(packageName);
        genericDataModel.setClassName(className);
        generics.add(genericDataModel);
        return this;
    }

    /**
     * 添加泛型(当前类)
     *
     * @param className
     * @return
     */
    public ExtendsClassDataModel genericThis(String className) {
        return generic(null, className);
    }


    /**
     * 获取包名
     *
     * @return
     */
    @Override
    public List<String> getPackageList() {
        List<String> packageList = new ArrayList<>();
        DataModelUtil.pushPackage(packageList, packageName);
        DataModelUtil.pushPackage(packageList, generics, GenericDataModel::getPackageName);
        return packageList;
    }
}
