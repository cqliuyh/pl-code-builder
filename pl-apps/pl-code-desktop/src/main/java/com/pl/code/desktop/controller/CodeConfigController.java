package com.pl.code.desktop.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pl.code.desktop.entity.dto.config.ConfigListDTO;
import com.pl.code.desktop.entity.dto.config.ConfigPageDTO;
import com.pl.code.desktop.entity.dto.config.ConfigSaveDTO;
import com.pl.code.desktop.entity.po.config.PlCodeConfig;
import com.pl.code.desktop.entity.vo.config.ConfigDetailsVO;
import com.pl.code.desktop.entity.vo.config.ConfigListVO;
import com.pl.code.desktop.service.config.IPlCodeConfigService;
import com.pl.core.utils.AssertUtil;
import com.pl.data.core.entity.PageVO;
import com.pl.data.core.response.Result;
import com.pl.data.core.utils.PoJoConverter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClasssName CodeConfigController
 * @Description 配置管理接口
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@RestController
@RequestMapping("config")
@Api(tags = "配置管理")
public class CodeConfigController {
    @Autowired
    private IPlCodeConfigService plCodeConfigService;


    @ApiOperation("添加配置")
    @PutMapping("add")
    public Result<Boolean> add(@Validated ConfigSaveDTO saveDTO) {
        PlCodeConfig codeConfig = PoJoConverter.copyProperties(saveDTO, PlCodeConfig.class);
        boolean state = plCodeConfigService.save(codeConfig);
        AssertUtil.isTrue(state, "保存失败");
        return Result.ok(state);
    }


    @ApiOperation("修改配置")
    @PostMapping("{id}")
    public Result<Boolean> update(@ApiParam("配置id") @PathVariable("id") Long id, @Validated ConfigSaveDTO saveDTO) {
        PlCodeConfig codeConfig = PoJoConverter.copyProperties(saveDTO, PlCodeConfig.class);
        codeConfig.setId(id);
        boolean state = plCodeConfigService.updateById(codeConfig);
        AssertUtil.isTrue(state, "修改失败");
        return Result.ok(state);
    }


    @ApiOperation("删除配置")
    @DeleteMapping("{id}")
    public Result<Boolean> remove(@ApiParam("配置id") @PathVariable("id") Long id) {
        boolean state = plCodeConfigService.removeById(id);
        AssertUtil.isTrue(state, "删除失败");
        return Result.ok(state);
    }


    @ApiOperation("配置详情")
    @GetMapping("{id}")
    public Result<ConfigDetailsVO> details(@ApiParam("配置id") @PathVariable("id") Long id) {
        PlCodeConfig entity = plCodeConfigService.getById(id);
        return Result.ok(PoJoConverter.copyProperties(entity, ConfigDetailsVO.class));
    }


    @ApiOperation("配置列表")
    @GetMapping("list")
    public Result<List<ConfigListVO>> list(ConfigListDTO listDTO) {
        LambdaQueryWrapper<PlCodeConfig> queryWrapper = createQueryWrapper(listDTO.getName());
        List<PlCodeConfig> dataList = plCodeConfigService.list(queryWrapper);
        return Result.ok(PoJoConverter.toList(dataList, PlCodeConfig::toListVO));
    }


    @ApiOperation("配置分页列表")
    @GetMapping("page")
    public Result<PageVO<ConfigListVO>> page(ConfigPageDTO pageDTO) {
        LambdaQueryWrapper<PlCodeConfig> queryWrapper = createQueryWrapper(pageDTO.getName());
        Page<PlCodeConfig> dataPage = plCodeConfigService.page(pageDTO.toPage(), queryWrapper);
        return Result.ok(PoJoConverter.toPage(dataPage, PlCodeConfig::toListVO));
    }


    /**
     * 创建公告查询包装器
     *
     * @param name
     * @return
     */
    private LambdaQueryWrapper<PlCodeConfig> createQueryWrapper(String name) {
        LambdaQueryWrapper<PlCodeConfig> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(PlCodeConfig::getId, PlCodeConfig::getName);
        queryWrapper.like(StrUtil.isNotBlank(name), PlCodeConfig::getName, name);
        queryWrapper.orderByDesc(PlCodeConfig::getCreateTime);
        return queryWrapper;
    }


}
