package com.pl.code.desktop.service.config.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pl.code.desktop.entity.po.config.PlCodeConfig;
import com.pl.code.desktop.mapper.config.PlCodeConfigMapper;
import com.pl.code.desktop.service.config.IPlCodeConfigService;
import org.springframework.stereotype.Service;

/**
 * @ClasssName PlCodeConfigServiceImpl
 * @Description 生成配置实现
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Service
public class PlCodeConfigServiceImpl extends ServiceImpl<PlCodeConfigMapper, PlCodeConfig> implements IPlCodeConfigService {
}
