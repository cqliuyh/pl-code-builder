package com.pl.code.desktop.mapper.datasource;

import com.pl.code.desktop.entity.bo.datasource.TableColumnBO;
import com.pl.code.desktop.entity.bo.datasource.TableInfoBO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @ClasssName DatabaseMapper
 * @Description 数据库mapper
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Mapper
public interface DatabaseMapper {
    /**
     * 查询表列表
     * @param tableName
     * @return
     */
    List<TableInfoBO> selectTableList(String tableName);

    /**
     * 查询表字段
     * @param tableName
     * @return
     */
    List<TableColumnBO> selectTableColumn(String tableName);

    /**
     * 查询数据库名称
     * @return
     */
    @Select("select database()")
    String selectDatabaseName();
}
