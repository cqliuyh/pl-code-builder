package com.pl.code.desktop.entity.dto.config;

import com.pl.data.core.entity.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName ConfigPageDTO
 * @Description 配置分页列表DTO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("配置分页列表DTO")
@Data
public class ConfigPageDTO extends PageDTO {
    @ApiModelProperty("配置名称")
    private String name;
}
