package com.pl.code.desktop.core.data.model.cls;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pl.code.desktop.core.data.interfaces.IPackageDataModel;
import com.pl.code.desktop.core.data.model.annotation.AnnotationDataModel;
import com.pl.code.desktop.core.data.model.factory.ClassFactory;
import com.pl.code.desktop.core.utils.DataModelUtil;
import lombok.Data;

import java.text.Collator;
import java.util.*;

/**
 * @ClasssName ClassDataModel
 * @Description 类数据模型
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
@Data
public class ClassDataModel<T> {

    // 包名
    private String packageName;
    // 包列表
    private List<String> importPackages = new ArrayList<>();
    // 注释
    private String description;
    // 注解
    private List<AnnotationDataModel> annotationList = new ArrayList<>();
    // 类名
    private String className;
    // 继承类
    private ExtendsClassDataModel extendsClass;
    // 实现类
    private List<ImplementsDataModel> implementsList = new ArrayList<>();
    // 扩展数据
    private T data;


    public ClassDataModel build() {
        formatImportPackages();
        return this;
    }


    /**
     * 格式化包名
     *
     * @return
     */
    public void formatImportPackages() {
        DataModelUtil.pushPackage(importPackages, annotationList, AnnotationDataModel::getPackageName);
        DataModelUtil.pushPackage(importPackages, extendsClass, ExtendsClassDataModel::getPackageName);
        DataModelUtil.pushPackage(importPackages, implementsList, ImplementsDataModel::getPackageName);
        Collections.sort(importPackages, Collator.getInstance(new Locale("es")));
    }

    public static void main(String[] args) {
        ClassFactory classFactory = ClassFactory
                .create("com.pl.Entity")
                // 注解
                .annotations(factory -> {
                    factory.create("com.baomidou.mybatisplus.annotation.TableId")
                            .property("type", "IdType.ASSIGN_ID", "com.baomidou.mybatisplus.annotation.IdType");
                })
                // 继承
                .extend(factory -> {
                    factory.create("com.baomidou.mybatisplus.extension.activerecord.Model").genericThis();
                })
                // 实现
                .implement(factory -> {
                    factory.create("com.baomidou.mybatisplus.extension.service.IService").genericThis();
                });
        classFactory.annotations(factory -> {
            factory.create("lombok.Data");
        });
        System.err.println(JSON.toJSONString(classFactory.build()));
    }


}
