package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClasssName EntityProperties
 * @Description Entity配置属性
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("Entity配置属性")
@Data
@NoArgsConstructor
public class EntityProperties {
    // 序列化
    @ApiModelProperty("是否开启序列化")
    private boolean serializable;
    // 是否继承父类
    @ApiModelProperty("是否继承父类")
    private boolean extendsSuper;
    // 父类包路径
    @ApiModelProperty("父类包路径")
    private String superClassPackage;
    // 父类泛型包路径
    @ApiModelProperty("父类泛型包路径")
    private List<String> superClassGenericPackage;
    // lombok配置
    @ApiModelProperty("lombok配置")
    private LombokProperties lombok;
}
