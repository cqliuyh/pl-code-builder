package com.pl.code.desktop.core.data.model.factory;

import com.pl.code.desktop.core.data.model.annotation.AnnotationDataModel;
import com.pl.code.desktop.core.data.model.cls.ClassDataModel;
import com.pl.code.desktop.core.data.model.cls.ExtendsClassDataModel;
import com.pl.code.desktop.core.data.model.cls.GenericDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;

/**
 * @ClasssName ExtendsFactory
 * @Description
 * @Author Liuyh
 * @Date 2021/10/21
 * @Version V0.0.1
 */
public class ExtendsFactory {

    private ClassDataModel classDataModel;

    public ExtendsFactory(ClassDataModel classDataModel) {
        this.classDataModel = classDataModel;
    }

    /**
     * 创建继承工厂
     *
     * @param packageName
     * @return
     */
    public ExtendsFactory create(String packageName) {
        String name = DataModelUtil.getClassNameByPackageName(packageName);
        return create(packageName, name);
    }

    /**
     * 创建继承工厂
     *
     * @param packageName
     * @param name
     * @return
     */
    public ExtendsFactory create(String packageName, String name) {
        ExtendsClassDataModel dataModel = ExtendsClassDataModel.build(packageName, name);
        classDataModel.setExtendsClass(dataModel);
        return this;
    }


    /**
     * 添加泛型
     *
     * @param packageName
     * @param className
     * @return
     */
    public ExtendsFactory generic(String packageName, String className) {
        GenericDataModel genericDataModel = new GenericDataModel();
        genericDataModel.setPackageName(packageName);
        genericDataModel.setClassName(className);
        classDataModel.getExtendsClass().generic(packageName, className);
        return this;
    }

    /**
     * 添加泛型(当前类)
     *
     * @param className
     * @return
     */
    public ExtendsFactory genericThis(String className) {
        return generic(null, className);
    }

    /**
     * 添加泛型(当前类)
     * @return
     */
    public ExtendsFactory genericThis() {
        return generic(null, classDataModel.getClassName());
    }

}
