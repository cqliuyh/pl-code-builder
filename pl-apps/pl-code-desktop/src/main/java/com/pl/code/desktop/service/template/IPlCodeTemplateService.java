package com.pl.code.desktop.service.template;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pl.code.desktop.entity.po.template.PlCodeTemplate;

/**
 * @ClasssName IPlCodeTemplateService
 * @Description 模板接口
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
public interface IPlCodeTemplateService extends IService<PlCodeTemplate> {
}
