package com.pl.code.desktop.core.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @ClasssName OrmTypeEnum
 * @Description 对象映射类型枚举
 * @Author Liuyh
 * @Date 2021/5/25
 * @Version V0.0.1
 */
@AllArgsConstructor
@Getter
public enum OrmTypeEnum {
    MYBATIS("mybatis"),
    MYBATIS_PLUS("mybatis-plus"),
    FLUENT_PLUS("mybatis-plus"),
    JPA("jpa");
    private String value;

    public boolean valueEquals(String value){
        return this.value.equalsIgnoreCase(value);
    }

    public static OrmTypeEnum ofValue(String value){
        return Arrays.stream(OrmTypeEnum.values()).filter(item -> item.valueEquals(value)).findFirst().orElse(null);
    }
}
