package com.pl.code.desktop.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pl.code.desktop.entity.dto.config.ConfigListDTO;
import com.pl.code.desktop.entity.dto.config.ConfigPageDTO;
import com.pl.code.desktop.entity.dto.config.ConfigSaveDTO;
import com.pl.code.desktop.entity.dto.template.TemplateListDTO;
import com.pl.code.desktop.entity.dto.template.TemplateSaveDTO;
import com.pl.code.desktop.entity.po.config.PlCodeConfig;
import com.pl.code.desktop.entity.po.template.PlCodeTemplate;
import com.pl.code.desktop.entity.vo.config.ConfigDetailsVO;
import com.pl.code.desktop.entity.vo.config.ConfigListVO;
import com.pl.code.desktop.entity.vo.template.TemplateConfigListVO;
import com.pl.code.desktop.entity.vo.template.TemplateDetailsVO;
import com.pl.code.desktop.service.template.IPlCodeTemplateService;
import com.pl.core.utils.AssertUtil;
import com.pl.data.core.entity.PageVO;
import com.pl.data.core.response.Result;
import com.pl.data.core.utils.PoJoConverter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClasssName TemplateController
 * @Description 模板管理接口
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */

@RestController
@RequestMapping("template")
@Api(tags = "模板管理")
public class TemplateController {
    @Autowired
    private IPlCodeTemplateService plCodeTemplateService;

    @ApiOperation("添加模板")
    @PutMapping("add")
    public Result<Boolean> add(@Validated TemplateSaveDTO saveDTO) {
        PlCodeTemplate entity = PoJoConverter.copyProperties(saveDTO, PlCodeTemplate.class);
        boolean state = plCodeTemplateService.save(entity);
        AssertUtil.isTrue(state, "保存失败");
        return Result.ok(state);
    }

    @ApiOperation("修改模板")
    @PostMapping("{id}")
    public Result<Boolean> update(@ApiParam("模板id") @PathVariable("id") Long id, @Validated TemplateSaveDTO saveDTO) {
        PlCodeTemplate entity = PoJoConverter.copyProperties(saveDTO, PlCodeTemplate.class);
        entity.setId(id);
        boolean state = plCodeTemplateService.updateById(entity);
        AssertUtil.isTrue(state, "修改失败");
        return Result.ok(state);
    }


    @ApiOperation("删除模板")
    @DeleteMapping("{id}")
    public Result<Boolean> remove(@ApiParam("模板id") @PathVariable("id") Long id) {
        boolean state = plCodeTemplateService.removeById(id);
        AssertUtil.isTrue(state, "删除失败");
        return Result.ok(state);
    }


    @ApiOperation("模板详情")
    @GetMapping("{id}")
    public Result<TemplateDetailsVO> details(@ApiParam("模板id") @PathVariable("id") Long id) {
        PlCodeTemplate entity = plCodeTemplateService.getById(id);
        return Result.ok(PoJoConverter.copyProperties(entity, TemplateDetailsVO.class));
    }


    @ApiOperation("模板列表")
    @GetMapping("list")
    public Result<List<TemplateConfigListVO>> list(TemplateListDTO listDTO) {
        LambdaQueryWrapper<PlCodeTemplate> queryWrapper = createQueryWrapper(listDTO.getName());
        List<PlCodeTemplate> dataList = plCodeTemplateService.list(queryWrapper);
        return Result.ok(PoJoConverter.toList(dataList, PlCodeTemplate::toListVO));
    }


    @ApiOperation("模板分页列表")
    @GetMapping("page")
    public Result<PageVO<TemplateConfigListVO>> page(ConfigPageDTO pageDTO) {
        LambdaQueryWrapper<PlCodeTemplate> queryWrapper = createQueryWrapper(pageDTO.getName());
        Page<PlCodeTemplate> dataPage = plCodeTemplateService.page(pageDTO.toPage(), queryWrapper);
        return Result.ok(PoJoConverter.toPage(dataPage, PlCodeTemplate::toListVO));
    }


    /**
     * 创建公告查询包装器
     *
     * @param name
     * @return
     */
    private LambdaQueryWrapper<PlCodeTemplate> createQueryWrapper(String name) {
        LambdaQueryWrapper<PlCodeTemplate> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(PlCodeTemplate::getId, PlCodeTemplate::getName, PlCodeTemplate::getDefaultFlag);
        queryWrapper.like(StrUtil.isNotBlank(name), PlCodeTemplate::getName, name);
        queryWrapper.orderByDesc(PlCodeTemplate::getCreateTime);
        return queryWrapper;
    }


}
