package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName WiredProperties
 * @Description 装配配置属性
 * @Author Liuyh
 * @Date 2021/9/26
 * @Version V0.0.1
 */
@ApiModel("装配配置属性")
@Data
public class WiredProperties {
    // 是否开启
    @ApiModelProperty("是否开启装配")
    private boolean enable;
    // 装配方式 autoWired:@AutoWired,resource:@Resource,constructor:构造方法
    @ApiModelProperty("装配方式 autoWired:@AutoWired,resource:@Resource,constructor:构造方法")
    private String type;
}
