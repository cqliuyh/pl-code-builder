package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClasssName TableProperties
 * @Description 表配置属性
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("表配置属性")
@Data
public class TableProperties {
    // 表前缀
    @ApiModelProperty("表前缀")
    private String prefix;
    // 公共字段
    @ApiModelProperty("公共字段")
    private List<String> common;
    // 隐藏字段
    @ApiModelProperty("隐藏字段")
    private List<String> hidden;
    // 逻辑删除字段
    @ApiModelProperty("逻辑删除字段")
    private List<String> logicDelete;
    // insert语句自动填充字段
    @ApiModelProperty("insert语句自动填充字段")
    private List<String> insertFill;
    // update语句自动填充字段
    @ApiModelProperty("update语句自动填充字段")
    private List<String> updateFill;
    // insert或update语句自动填充字段
    @ApiModelProperty("insert或update语句自动填充字段")
    private List<String> insertOrUpdateFill;
}
