package com.pl.code.desktop.core.data.model.annotation;

import com.pl.code.desktop.core.data.model.KeyValueDataModel;
import lombok.Data;

/**
 * @ClasssName AnnotationPropertyDataModel
 * @Description 注解属性数据模型
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
@Data
public class AnnotationPropertyDataModel extends KeyValueDataModel {
    // 是否添加引号
    private boolean quotation;
}
