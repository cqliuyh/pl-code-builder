package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @ClasssName LombokProperties
 * @Description lombok配置属性
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("lombok配置属性")
@Data
public class LombokProperties {

    // 是否开启lombok
    @ApiModelProperty("是否开启lombok")
    private boolean enable;
    // @builder注解
    @ApiModelProperty("@builder注解")
    private boolean builder;
    // @data注解
    @ApiModelProperty("@data注解")
    private boolean data;
    // @getter注解
    @ApiModelProperty("@getter注解")
    private boolean getter;
    // @setter注解
    @ApiModelProperty("@setter注解")
    private boolean setter;
    // @ToString注解
    @ApiModelProperty("@ToString注解")
    private boolean toStr;
    // @equalsAndHashCode注解
    @ApiModelProperty("@equalsAndHashCode注解")
    private boolean equalsAndHashCode;
    // @allArgsConstructor注解
    @ApiModelProperty("@allArgsConstructor注解")
    private boolean allArgsConstructor;
    // @noAllArgsConstructor 注解
    @ApiModelProperty("@noAllArgsConstructor")
    private boolean noAllArgsConstructor;
    // @accessors 注解配置
    @ApiModelProperty("@accessors 注解配置")
    private Accessors accessors;


    @Data
    @ApiModel("@accessors 注解配置")
    public static class Accessors {
        // 是否开启
        @ApiModelProperty("是否开启")
        private boolean enable;
        // 模式
        @ApiModelProperty("模式")
        private String mode;
        // 模式属性
        @ApiModelProperty("模式属性")
        private String prefixModeValue;
    }

    /**
     * 是否开启lombok注解
     * @return
     */
    public boolean hasAnnotation() {
        return enable && (builder || data || getter || setter || toStr || equalsAndHashCode || allArgsConstructor || noAllArgsConstructor || isEnableAccessors());
    }

    /**
     * 是否开启@accessors注解
     * @return
     */
    public boolean isEnableAccessors() {
        return accessors != null && accessors.enable;
    }

    /**
     * 获取@accessors注解的属性
     * @return
     */
    public String getAccessorsPropertity() {
        if (StringUtils.isBlank(accessors.mode)) {
            return null;
        }
        if ("prefix".equalsIgnoreCase(accessors.mode)) {
            if (StringUtils.isBlank(accessors.prefixModeValue)){
                return null;
            } else {
                return "prefix=" + accessors.prefixModeValue;
            }

        }
        return accessors.mode + "=true";

    }
}
