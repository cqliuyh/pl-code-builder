package com.pl.code.desktop.mapper.config;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pl.code.desktop.entity.po.config.PlCodeConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClasssName PlCodeConfigMapper
 * @Description
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Mapper
public interface PlCodeConfigMapper extends BaseMapper<PlCodeConfig> {
}
