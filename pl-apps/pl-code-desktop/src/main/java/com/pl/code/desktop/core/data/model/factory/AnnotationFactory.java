package com.pl.code.desktop.core.data.model.factory;

import com.pl.code.desktop.core.data.model.annotation.AnnotationDataModel;
import com.pl.code.desktop.core.data.model.annotation.AnnotationPropertyDataModel;
import com.pl.code.desktop.core.data.model.cls.ClassDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;
import lombok.Data;

import java.util.List;

/**
 * 注解工厂类
 */
@Data
public class AnnotationFactory {
    private ClassDataModel classDataModel;

    public AnnotationFactory(ClassDataModel classDataModel) {
        this.classDataModel = classDataModel;
    }


    /**
     * 创建一个注解
     *
     * @param packageName
     * @return
     */
    public AnnotationFactory create(String packageName) {
        String name = DataModelUtil.getClassNameByPackageName(packageName);
        return create(packageName, name);
    }

    /**
     * 创建一个注解
     *
     * @param packageName
     * @param name
     * @return
     */
    public AnnotationFactory create(String packageName, String name) {
        AnnotationDataModel annotationDataModel = new AnnotationDataModel();
        annotationDataModel.setPackageName(packageName);
        annotationDataModel.setName(name);
        classDataModel.getAnnotationList().add(annotationDataModel);
        return this;
    }


    /**
     * 添加属性
     *
     * @param property
     * @return
     */
    public AnnotationFactory property(AnnotationPropertyDataModel property) {
        List<AnnotationDataModel> annotationDataModelList = classDataModel.getAnnotationList();
        annotationDataModelList.get(annotationDataModelList.size() - 1).property(property);
        return this;
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @param packageName
     * @param quotation
     * @return
     */
    public AnnotationFactory property(String key, String value, String packageName, boolean quotation) {
        AnnotationPropertyDataModel property = new AnnotationPropertyDataModel();
        property.setKey(key);
        property.setValue(value);
        property.setPackageName(packageName);
        property.setQuotation(quotation);
        return property(property);
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @return
     */
    public AnnotationFactory property(String key, String value) {
        return property(key, value, null);
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @return
     */
    public AnnotationFactory property(String key, String value, String packageName) {
        return property(key, value, packageName, false);
    }

    /**
     * 添加属性
     *
     * @param key
     * @param value
     * @return
     */
    public AnnotationFactory quotationProperty(String key, String value) {
        AnnotationPropertyDataModel property = new AnnotationPropertyDataModel();
        property.setKey(key);
        property.setValue(value);
        property.setQuotation(true);
        return property(property);
    }


    public AnnotationFactory and() {
        return new AnnotationFactory(classDataModel);
    }

    public ClassDataModel build() {
        return classDataModel;
    }
}