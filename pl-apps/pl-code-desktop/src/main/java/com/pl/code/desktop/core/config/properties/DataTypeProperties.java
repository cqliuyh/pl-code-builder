package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName DataTypeProperties
 * @Description 数据类型映射配置属性
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("数据类型映射配置属性")
@Data
public class DataTypeProperties {
    // 数据库数据类型
    @ApiModelProperty("数据库数据类型")
    private String columnType;
    // java数据类型
    @ApiModelProperty("java数据类型")
    private String javaType;
    // 是否需要导包
    @ApiModelProperty("是否需要导包")
    private boolean importPackage;
    // 包名
    @ApiModelProperty("包名")
    private String packageName;
}
