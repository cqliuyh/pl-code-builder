package com.pl.code.desktop.core.web;

import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import com.pl.code.desktop.service.datasource.IDataSourceService;
import com.pl.core.utils.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClasssName AbstractDataSourceController
 * @Description 数据源抽象类
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
public abstract class AbstractDataSourceController {
    @Autowired
    private IDataSourceService dataSourceService;

    /**
     * 通过数据源id或去数据源
     * @param id
     * @return
     */
    public PlDataSource getDataSourceById(Long id) {
        PlDataSource dataSource = dataSourceService.getById(id);
        AssertUtil.notNull(dataSource, "未查询到数据源信息");
        return dataSource;
    }
}
