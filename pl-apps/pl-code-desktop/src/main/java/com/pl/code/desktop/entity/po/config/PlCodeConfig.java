package com.pl.code.desktop.entity.po.config;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pl.code.desktop.entity.vo.config.ConfigListVO;
import com.pl.data.core.entity.AbstractPlEntity;
import com.pl.data.core.utils.PoJoConverter;
import lombok.Data;

/**
 * @ClasssName PlCodeConfig
 * @Description 生成配置
 * @Author Liuyh
 * @Date 2021/5/15
 * @Version V0.0.1
 */
@Data
@TableName("pl_code_config")
public class PlCodeConfig extends AbstractPlEntity<PlCodeConfig> {
    // 主键/配置id
    @TableId(type = IdType.AUTO, value = "id")
    private Long id;
    // 配置名称
    private String name;
    // 配置内容
    private String content;

    /**
     * 转换成列表VO
     * @return
     */
    public ConfigListVO toListVO() {
        ConfigListVO vo = PoJoConverter.copyProperties(this, ConfigListVO.class);
        return vo;
    }

}