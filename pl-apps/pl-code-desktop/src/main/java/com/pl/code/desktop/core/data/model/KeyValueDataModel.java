package com.pl.code.desktop.core.data.model;

import cn.hutool.core.util.StrUtil;
import com.pl.code.desktop.core.data.interfaces.IPackageDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClasssName KeyValueDataModel
 * @Description 键值对数据模型
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
@Data
public class KeyValueDataModel implements IPackageDataModel {
    // 包名
    private String packageName;
    // 键
    private String key;
    // 值
    private String value;

    /**
     * 获取包名
     *
     * @return
     */
    @Override
    public List<String> getPackageList() {
        List<String> packageList = new ArrayList<>();
        DataModelUtil.pushPackage(packageList, packageName);
        return packageList;
    }


}
