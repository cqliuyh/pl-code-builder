package com.pl.code.desktop.core.config;

import com.pl.code.desktop.core.config.properties.*;
import com.pl.code.desktop.core.utils.YamlPropertyResourceFactory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClasssName GeneratorConfiguration
 * @Description 代码生成配置
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Component
@ConfigurationProperties(prefix = "generator")
@PropertySource(value = "classpath:generator.yml", factory = YamlPropertyResourceFactory.class)
@Data
@ApiModel("代码生成配置")
public class GeneratorConfiguration {
    // 包名
    @ApiModelProperty("包名")
    private String packageName;
    // 作者名称
    @ApiModelProperty("作者名称")
    private String author;
    // 装配
    @ApiModelProperty("装配")
    private WiredProperties wired;
    // swagger
    @ApiModelProperty("swagger")
    private SwaggerProperties swagger;
    // 类型映射
    @ApiModelProperty("类型映射")
    private List<DataTypeProperties> dataType;
    // 表配置
    @ApiModelProperty("表配置")
    private TableProperties table;
    // 实体配置
    @ApiModelProperty("实体配置")
    private EntityProperties entity;
    // ORM配置
    @ApiModelProperty("ORM配置")
    private OrmProperties orm;
    // Controller配置
    @ApiModelProperty("Controller配置")
    private ControllerProperties controller;
}
