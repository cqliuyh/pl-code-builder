package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName ResponseEntityProperties
 * @Description 响应实体配置属性
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@ApiModel("响应实体配置属性")
@Data
public class ResponseEntityProperties {
    // 包名
    @ApiModelProperty("包名")
    private String packageName;
    // 成功方法名
    @ApiModelProperty("成功方法名")
    private String successMethod;
    // 失败/错误方法名
    @ApiModelProperty("失败/错误方法名")
    private String errorMethod;
}
