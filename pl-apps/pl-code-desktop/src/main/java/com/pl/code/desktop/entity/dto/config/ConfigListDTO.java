package com.pl.code.desktop.entity.dto.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName ConfigListDTO
 * @Description 配置列表DTO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("配置列表DTO")
@Data
public class ConfigListDTO {

    @ApiModelProperty("配置名称")
    private String name;

}
