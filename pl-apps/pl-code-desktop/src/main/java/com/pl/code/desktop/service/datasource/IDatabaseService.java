package com.pl.code.desktop.service.datasource;

import com.pl.code.desktop.entity.bo.datasource.DatabaseInfoBO;
import com.pl.code.desktop.entity.bo.datasource.TableColumnBO;
import com.pl.code.desktop.entity.bo.datasource.TableInfoBO;
import com.pl.code.desktop.entity.vo.datasource.TableInfoVO;

import java.util.List;

/**
 * @ClasssName IDatabaseService
 * @Description 数据库接口
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
public interface IDatabaseService {

    /**
     * 获取数据库详情
     *
     * @param dsId
     * @param dsName
     * @return
     */
    DatabaseInfoBO getDatabaseInfo(Long dsId, String dsName);

    /**
     * 获取表详情
     * @param tableName
     * @param dsName
     * @return
     */
    TableInfoVO getTableDetails(String tableName, String dsName);

    /**
     * 获取表信息
     * @param tableName
     * @param dsName
     * @return
     */
    TableInfoBO getTableInfo(String tableName, String dsName);

    /**
     * 获取表字段
     * @param tableName
     * @param dsName
     * @return
     */
    List<TableColumnBO> getTableColumn(String tableName, String dsName);
}
