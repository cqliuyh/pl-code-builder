package com.pl.code.desktop.service.template.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pl.code.desktop.entity.po.template.PlCodeTemplate;
import com.pl.code.desktop.mapper.template.PlCodeTemplateMapper;
import com.pl.code.desktop.service.template.IPlCodeTemplateService;
import org.springframework.stereotype.Service;

/**
 * @ClasssName PlCodeTemplateImpl
 * @Description 模板实现类
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Service
public class PlCodeTemplateImpl extends ServiceImpl<PlCodeTemplateMapper, PlCodeTemplate> implements IPlCodeTemplateService {
}
