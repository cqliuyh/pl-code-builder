package com.pl.code.desktop.service.datasource.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.pl.code.desktop.entity.bo.datasource.DatabaseInfoBO;
import com.pl.code.desktop.entity.bo.datasource.TableColumnBO;
import com.pl.code.desktop.entity.bo.datasource.TableInfoBO;
import com.pl.code.desktop.entity.vo.datasource.TableInfoVO;
import com.pl.code.desktop.mapper.datasource.DatabaseMapper;
import com.pl.code.desktop.service.datasource.IDatabaseService;
import com.pl.core.utils.AssertUtil;
import com.pl.data.core.utils.PoJoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClasssName DatabaseServiceImpl
 * @Description 数据库接口实现
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@DS("#last")
@Service
public class DatabaseServiceImpl implements IDatabaseService {
    @Autowired
    private DatabaseMapper databaseMapper;

    /**
     * 获取数据库详情
     *
     * @param dsId
     * @param dsName
     * @return
     */
    @Override
    public DatabaseInfoBO getDatabaseInfo(Long dsId, String dsName) {
        DatabaseInfoBO dataBaseInfoBO = new DatabaseInfoBO();
        dataBaseInfoBO.setDsId(dsId);
        dataBaseInfoBO.setDsName(dsName);
        dataBaseInfoBO.setDbName(databaseMapper.selectDatabaseName());
        dataBaseInfoBO.setTables(databaseMapper.selectTableList(null));
        return dataBaseInfoBO;
    }

    /**
     * 获取表详情
     *
     * @param tableName
     * @param dsName
     * @return
     */
    @Override
    public TableInfoVO getTableDetails(String tableName, String dsName) {
        List<TableInfoBO> tableInfoList = databaseMapper.selectTableList(tableName);
        AssertUtil.notEmpty(tableInfoList, "未查询到表信息");
        TableInfoBO tableInfoBO = tableInfoList.get(0);

        List<TableColumnBO> columnList = databaseMapper.selectTableColumn(tableName);

        TableInfoVO vo = PoJoConverter.copyProperties(tableInfoBO, TableInfoVO.class);
        vo.setColumns(columnList);
        vo.setDsName(dsName);
        return vo;
    }

    /**
     * 获取表信息
     * @param tableName
     * @param dsName
     * @return
     */
    @Override
    public TableInfoBO getTableInfo(String tableName, String dsName) {
        List<TableInfoBO> tableInfoList = databaseMapper.selectTableList(tableName);
        AssertUtil.notEmpty(tableInfoList, "未查询到表信息");
        TableInfoBO tableInfoBO = tableInfoList.get(0);
        return tableInfoBO;
    }

    /**
     * 获取表字段
     *
     * @param tableName
     * @param dsName
     * @return
     */
    @Override
    public List<TableColumnBO> getTableColumn(String tableName, String dsName) {
        List<TableColumnBO> columnList = databaseMapper.selectTableColumn(tableName);
        return columnList;
    }
}
