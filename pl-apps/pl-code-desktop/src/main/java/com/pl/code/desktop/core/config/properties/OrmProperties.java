package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClasssName OrmProperties
 * @Description ORM 配置
 * @Author Liuyh
 * @Date 2021/9/26
 * @Version V0.0.1
 */
@ApiModel("ORM 配置")
@Data
public class OrmProperties {
    // ORM类型
    @ApiModelProperty("ORM框架类型 mybatis,mybatis-plus,fluent-mybatis,jpa")
    private String type;
    // 主键类型
    @ApiModelProperty("id类型(mybatis-plus下有效) auto:数据库ID自增,none:未设置主键,input:用户输入ID,assign_id:雪花算法Id,assign_uuid:uuid")
    private String idType;
    // 是否生成xml
    @ApiModelProperty("是否生成xml")
    private boolean mapperXml;
    // 注解
    @ApiModelProperty("注解")
    private String annotation;
    // 要生成的方法
    @ApiModelProperty("要生成的方法")
    private List<String> method;
}
