package com.pl.code.desktop.core.config;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.pl.code.desktop.core.config.properties.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClasssName ConfigurationAdapter
 * @Description 配置适配器
 * @Author Liuyh
 * @Date 2021/9/26
 * @Version V0.0.1
 */
@Component
public class ConfigurationAdapter {
    @Autowired
    private GeneratorConfiguration defaultConfiguration;

    public GeneratorConfiguration getDefaultConfiguration(){
        return defaultConfiguration;
    }

    /**
     * 获取实体配置
     *
     * @param configuration
     * @return
     */
    public EntityProperties entity(GeneratorConfiguration configuration) {
        if (ObjectUtil.isNull(configuration) || ObjectUtil.isNull(configuration.getEntity())) {
            return defaultConfiguration.getEntity();
        } else {
            return configuration.getEntity();
        }
    }

    /**
     * 获取lombok配置
     *
     * @param configuration
     * @return
     */
    public LombokProperties lombok(GeneratorConfiguration configuration) {
        if (ObjectUtil.isNull(configuration) || ObjectUtil.isNull(configuration.getEntity()) || ObjectUtil.isNull(configuration.getEntity().getLombok())) {
            return defaultConfiguration.getEntity().getLombok();
        } else {
            return configuration.getEntity().getLombok();
        }
    }

    /**
     * swagger配置
     * @param configuration
     * @return
     */
    public SwaggerProperties swagger(GeneratorConfiguration configuration) {
        if (ObjectUtil.isNull(configuration) || ObjectUtil.isNull(configuration.getSwagger())) {
            return defaultConfiguration.getSwagger();
        } else {
            return configuration.getSwagger();
        }
    }

    /**
     * orm配置
     * @param configuration
     * @return
     */
    public OrmProperties orm(GeneratorConfiguration configuration){
        if (ObjectUtil.isNull(configuration) || ObjectUtil.isNull(configuration.getOrm())) {
            return defaultConfiguration.getOrm();
        } else {
            return configuration.getOrm();
        }
    }

    /**
     * 表配置
     * @param configuration
     * @return
     */
    public TableProperties table(GeneratorConfiguration configuration){
        if (ObjectUtil.isNull(configuration) || ObjectUtil.isNull(configuration.getTable())) {
            return defaultConfiguration.getTable();
        } else {
            return configuration.getTable();
        }
    }

    /**
     * 表配置
     * @param configuration
     * @return
     */
    public List<DataTypeProperties> dataType(GeneratorConfiguration configuration){
        if (ObjectUtil.isNull(configuration) || CollUtil.isNotEmpty(configuration.getDataType())) {
            return defaultConfiguration.getDataType();
        } else {
            return configuration.getDataType();
        }
    }
}
