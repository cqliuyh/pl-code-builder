package com.pl.code.desktop.service.config;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pl.code.desktop.entity.po.config.PlCodeConfig;

/**
 * @ClasssName IPlCodeConfigService
 * @Description 生成配置接口
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
public interface IPlCodeConfigService extends IService<PlCodeConfig> {
}
