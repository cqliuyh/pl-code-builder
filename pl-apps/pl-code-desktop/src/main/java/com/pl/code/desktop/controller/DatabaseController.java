package com.pl.code.desktop.controller;

import com.pl.code.desktop.core.web.AbstractDataSourceController;
import com.pl.code.desktop.entity.bo.datasource.DatabaseInfoBO;
import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import com.pl.code.desktop.entity.vo.datasource.TableInfoVO;
import com.pl.code.desktop.service.datasource.IDatabaseService;
import com.pl.data.core.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClasssName DatabaseController
 * @Description 数据库相关接口
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */

@RestController
@RequestMapping("database")
@Api(tags = "数据库相关接口")
public class DatabaseController extends AbstractDataSourceController {
    @Autowired
    private IDatabaseService databaseService;

    @ApiOperation("数据库详情")
    @GetMapping("{id}")
    public Result<DatabaseInfoBO> getDatabaseInfo(@PathVariable("id") @ApiParam(value = "数据源id", required = true) Long id) {
        PlDataSource dataSource = getDataSourceById(id);
        return Result.ok(databaseService.getDatabaseInfo(dataSource.getId(), dataSource.getName()));
    }

    @ApiOperation("表详情")
    @GetMapping("{id}/table/{tableName}")
    public Result<TableInfoVO> getTableDetails(
            @PathVariable("id") @ApiParam(value = "数据源id", required = true) Long id,
            @PathVariable("tableName") @ApiParam(value = "表名", required = true) String tableName) {
        PlDataSource dataSource = getDataSourceById(id);
        return Result.ok(databaseService.getTableDetails(tableName, dataSource.getName()));
    }
}
