package com.pl.code.desktop.entity.vo.template;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ClasssName TemplateConfigListVO
 * @Description 模板列表VO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("模板列表VO")
@Data
public class TemplateConfigListVO {

    // 配置id
    @ApiModelProperty("配置id")
    private Long id;
    // 是否为默认配置
    @ApiModelProperty("是否为默认配置")
    private Boolean defaultFlag;

    // 配置名称
    @NotBlank(message = "配置名称不能为空")
    private String name;
}
