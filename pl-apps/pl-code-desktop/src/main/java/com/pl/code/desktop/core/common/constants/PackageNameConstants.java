package com.pl.code.desktop.core.common.constants;

/**
 * @ClasssName PackageNameConstants
 * @Description 包名常量
 * @Author Liuyh
 * @Date 2021/9/28
 * @Version V0.0.1
 */
public interface PackageNameConstants {

    /**
     * mybatis-plus相关包名
     */
    interface MybatisPlus {
        // Model
        String model = "com.baomidou.mybatisplus.extension.activerecord.Model";
        // @TableId
        String tableId = "com.baomidou.mybatisplus.annotation.TableId";
        // IdType
        String idType = "com.baomidou.mybatisplus.annotation.IdType";
        // TableField
        String tableField = "com.baomidou.mybatisplus.annotation.TableField";
        // FieldFill
        String fieldFill = "com.baomidou.mybatisplus.annotation.FieldFill";
        // TableLogic
        String tableLogic = "com.baomidou.mybatisplus.annotation.TableLogic";
    }


    /**
     * swagger相关包名
     */
    interface Swagger {
        // ApiModelProperty
        String apiModelProperty = "io.swagger.annotations.ApiModelProperty";
        // ApiModel
        String ApiModel = "io.swagger.annotations.ApiModel";

    }


    interface Lombok {
        String all = "lombok.*";
    }
}
