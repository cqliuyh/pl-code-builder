package com.pl.code.desktop.core.data.model.factory;

import com.pl.code.desktop.core.data.interfaces.IClassAnnotationFactory;
import com.pl.code.desktop.core.data.interfaces.IExtendsFactory;
import com.pl.code.desktop.core.data.interfaces.IImplementsFactory;
import com.pl.code.desktop.core.data.model.cls.ClassDataModel;
import com.pl.code.desktop.core.utils.DataModelUtil;

/**
 * @ClasssName ClassFactory
 * @Description
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */

public class ClassFactory {

    private ClassDataModel classDataModel;


    public static ClassFactory create(String packageName) {
        String className = DataModelUtil.getClassNameByPackageName(packageName);
        return create(packageName, className);
    }

    public static ClassFactory create(String packageName, String className) {
        ClassFactory factory = new ClassFactory();
        ClassDataModel classDataModel = new ClassDataModel();
        classDataModel.setPackageName(packageName);
        classDataModel.setClassName(className);
        factory.classDataModel = classDataModel;
        return factory;
    }


    /**
     * 注解工厂
     * @param ifactory
     * @return
     */
    public ClassFactory annotations(IClassAnnotationFactory ifactory) {
        AnnotationFactory factory = new AnnotationFactory(classDataModel);
        ifactory.annotations(factory);
        return this;
    }

    /**
     * 继承工厂
     * @param ifactory
     * @return
     */
    public ClassFactory extend(IExtendsFactory ifactory) {
        ExtendsFactory factory = new ExtendsFactory(classDataModel);
        ifactory.extend(factory);
        return this;
    }

    public ClassFactory implement(IImplementsFactory ifactory) {
        ImplementsFactory factory = new ImplementsFactory(classDataModel);
        ifactory.implement(factory);
        return this;
    }

    public ClassFactory data(){
        return this;
    }




    public ClassDataModel build() {
        return classDataModel.build();
    }
}
