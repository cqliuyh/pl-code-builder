package com.pl.code.desktop.core.data.interfaces;

import com.pl.code.desktop.core.data.model.factory.ImplementsFactory;

/**
 * @ClasssName IImplementsFactory
 * @Description
 * @Author Liuyh
 * @Date 2021/10/21
 * @Version V0.0.1
 */
public interface IImplementsFactory {
    /**
     * 实现接口
     * @param factory
     */
    void implement(ImplementsFactory factory);
}
