package com.pl.code.desktop.core.config;

import lombok.Data;

/**
 * @ClasssName CustomConfigurationProperties
 * @Description 自定义的属性
 * @Author Liuyh
 * @Date 2021/9/26
 * @Version V0.0.1
 */
@Data
public class CustomConfigurationProperties {
    // 表前缀
    private String tablePrefix;
    // 作者
    private String author;
    // 包名
    private String packageName;
    // 模块名称
    private String moduleName;
    // 代码注释
    private String codeComment;
    // 生成风格 mybatis,mybatis-plus,fluent-mybatis,jpa
    private String style;
    // 是否装配
    private boolean wired;
    // 装配类型
    private String wiredType;
    // swagger
    private boolean swagger;
}
