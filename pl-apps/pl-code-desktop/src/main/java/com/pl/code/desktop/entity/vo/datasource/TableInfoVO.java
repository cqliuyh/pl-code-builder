package com.pl.code.desktop.entity.vo.datasource;

import com.pl.code.desktop.entity.bo.datasource.TableColumnBO;
import com.pl.code.desktop.entity.bo.datasource.TableInfoBO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClasssName TableInfoVO
 * @Description 表信息VO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Data
@ApiModel("表信息VO")
public class TableInfoVO extends TableInfoBO {

    @ApiModelProperty("表字段列表")
    private List<TableColumnBO> columns;
}
