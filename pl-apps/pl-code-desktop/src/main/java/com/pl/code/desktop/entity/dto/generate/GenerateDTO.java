package com.pl.code.desktop.entity.dto.generate;

import cn.hutool.core.bean.BeanUtil;
import com.pl.code.desktop.core.config.CustomConfigurationProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @ClasssName GenerateDTO
 * @Description 代码生成配置DTO
 * @Author Liuyh
 * @Date 2021/5/17
 * @Version V0.0.1
 */
@Data
public class GenerateDTO {
    // 数据源名称
    private String dsName;
    // 表名
    private String tableName;
    // 表前缀
    private String tablePrefix;
    // 作者
    private String author;
    // 包名
    private String packageName;
    // 模块名称
    private String moduleName;
    // 代码注释
    private String codeAnnotation;
    // 配置id
    private Long configId;
    @NotNull(message = "请选择模板")
    private List<String> templates;
    // 生成风格 pl-web/mybatis-plus/mybatis
    private String style;
    // 是否装配
    private boolean wired;
    // 装配类型
    private String wiredType;
    // swagger
    private boolean swagger;

    /**
     * 转换成自定义配置
     * @return
     */
    public CustomConfigurationProperties toCustomConfiguration() {
        CustomConfigurationProperties configurationProperties = new CustomConfigurationProperties();
        BeanUtil.copyProperties(this, configurationProperties);
        return configurationProperties;
    }
}
