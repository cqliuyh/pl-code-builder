package com.pl.code.desktop.service.datasource.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pl.code.desktop.core.DataSourceHelper;
import com.pl.code.desktop.entity.dto.datasource.DataSourceListDTO;
import com.pl.code.desktop.entity.dto.datasource.DataSourcePageDTO;
import com.pl.code.desktop.entity.dto.datasource.DataSourceSaveDTO;
import com.pl.code.desktop.entity.po.datasource.PlDataSource;
import com.pl.code.desktop.entity.vo.datasource.DataSourceListVO;
import com.pl.code.desktop.entity.vo.datasource.DataSourcePageVO;
import com.pl.code.desktop.mapper.datasource.DataSourceMapper;
import com.pl.code.desktop.service.datasource.IDataSourceService;
import com.pl.core.utils.AssertUtil;
import com.pl.data.core.entity.PageVO;
import com.pl.data.core.utils.PoJoConverter;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClasssName DataSourceServiceImpl
 * @Description 数据源实现类
 * @Author Liuyh
 * @Date 2021/9/16
 * @Version V0.0.1
 */
@Service
public class DataSourceServiceImpl extends ServiceImpl<DataSourceMapper, PlDataSource> implements IDataSourceService {
    /**
     * 添加数据源
     *
     * @param dataSource
     * @return
     */
    @Override
    public boolean add(DataSourceSaveDTO dataSource) {
        PlDataSource entity = dataSource.toPlDataSource();
        boolean state = save(entity);
        AssertUtil.isTrue(state, "添加失败");
        return state;
    }

    /**
     * 删除数据源
     *
     * @param id
     * @return
     */
    @Override
    public boolean remove(Long id) {
        PlDataSource entity = getById(id);
        AssertUtil.notNull(entity, "数据源不存在");
        DataSourceHelper.close(entity.getName());
        return removeById(id);
    }

    /**
     * 修改数据源
     *
     * @param updateDTO
     * @return
     */
    @Override
    public boolean update(Long id, DataSourceSaveDTO updateDTO) {
        PlDataSource entity = updateDTO.toPlDataSource();
        entity.setId(id);
        boolean state = updateById(entity);
        AssertUtil.isTrue(state, "修改失败");
        return state;
    }

    /**
     * 列表
     *
     * @param listDTO
     * @return
     */
    @Override
    public List<DataSourceListVO> list(DataSourceListDTO listDTO) {
        LambdaQueryWrapper<PlDataSource> queryWrapper = createQueryWrapper(listDTO.getName());
        List<DataSourceListVO> dataSourceListVOList = PoJoConverter.toList(list(queryWrapper),PlDataSource::toListVo);
        return dataSourceListVOList;
    }

    /**
     * 分页列表
     *
     * @param pageDTO
     * @return
     */
    @Override
    public PageVO<DataSourcePageVO> page(DataSourcePageDTO pageDTO) {
        LambdaQueryWrapper<PlDataSource> queryWrapper = createQueryWrapper(pageDTO.getName());
        IPage<PlDataSource> pageData = page(pageDTO.toPage(),queryWrapper);
        PageVO<DataSourcePageVO> pageDataVo = PoJoConverter.toPage(pageData,PlDataSource::toListVo);
        return pageDataVo;
    }

    /**
     * 通过数据源名称获取数据源
     *
     * @param name
     * @return
     */
    @Override
    public PlDataSource getByDsName(String name) {
        LambdaQueryWrapper<PlDataSource> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(StrUtil.isNotBlank(name),PlDataSource::getName,name);
        return getOne(queryWrapper);
    }

    /**
     * 创建公告查询包装器
     * @param name
     * @return
     */
    private LambdaQueryWrapper<PlDataSource> createQueryWrapper(String name){
        LambdaQueryWrapper<PlDataSource> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(StrUtil.isNotBlank(name),PlDataSource::getName,name);
        queryWrapper.orderByDesc(PlDataSource::getCreateTime);
        return queryWrapper;
    }
}
