package com.pl.code.desktop.core.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @ClasssName OrmTypeEnum
 * @Description swagger模式枚举
 * @Author Liuyh
 * @Date 2021/5/25
 * @Version V0.0.1
 */
@AllArgsConstructor
@Getter
public enum SwaggerModelEnum {
    // 完整模式
    FULL("full"),
    // 简单模式
    SIMPLE("simple");
    private String value;

    public boolean valueEquals(String value){
        return this.value.equalsIgnoreCase(value);
    }

    public static SwaggerModelEnum ofValue(String value){
        return Arrays.stream(SwaggerModelEnum.values()).filter(item -> item.valueEquals(value)).findFirst().orElse(null);
    }
}
