package com.pl.code.desktop.entity.vo.datasource;

import lombok.Data;

/**
 * @ClasssName DataSourcePageVO
 * @Description 数据源分页列表VO
 * @Author Liuyh
 * @Date 2021/9/17
 * @Version V0.0.1
 */
@Data
public class DataSourcePageVO extends DataSourceListVO{
}
