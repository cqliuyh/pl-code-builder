package com.pl.code.desktop.entity.bo.datasource;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClasssName DatabaseInfoBO
 * @Description 数据库信息BO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("数据库信息BO")
@Data
public class DatabaseInfoBO {

    @ApiModelProperty("数据源id")
    private Long dsId;

    @ApiModelProperty("数据源名称")
    private String dsName;

    @ApiModelProperty("数据库名称")
    private String dbName;

    @ApiModelProperty("表")
    private List<TableInfoBO> tables;
}
