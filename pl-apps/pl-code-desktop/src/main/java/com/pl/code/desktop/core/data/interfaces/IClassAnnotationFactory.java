package com.pl.code.desktop.core.data.interfaces;

import com.pl.code.desktop.core.data.model.factory.AnnotationFactory;

/**
 * @ClasssName IClassAnnotationFactory
 * @Description 类注解工厂接口
 * @Author Liuyh
 * @Date 2021/10/21
 * @Version V0.0.1
 */
public interface IClassAnnotationFactory {
    /**
     * 类注解
     * @param factory
     */
    void annotations(AnnotationFactory factory);
}
