package com.pl.code.desktop.entity.vo.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName ConfigListVO
 * @Description 配置列表VO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("配置列表VO")
@Data
public class ConfigListVO {
    @ApiModelProperty("配置id")
    private Long id;

    @ApiModelProperty("配置名称")
    private String name;
}
