package com.pl.code.desktop.entity.vo.template;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ClasssName TemplateDetailsVO
 * @Description 模板详情VO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("模板详情VO")
@Data
public class TemplateDetailsVO {

    // 配置id
    @ApiModelProperty("配置id")
    private Long id;
    // 是否为默认配置
    @ApiModelProperty("是否为默认配置")
    private Boolean defaultFlag;

    // 配置名称
    @NotBlank(message = "配置名称不能为空")
    private String name;

    // Entity模板
    @ApiModelProperty("Entity模板")
    private String entity;

    // Mapper模板
    @ApiModelProperty("Mapper模板")
    private String mapper;

    // MapperXml模板
    @ApiModelProperty("MapperXml模板")
    private String mapperXml;

    // Service模板
    @ApiModelProperty("Service模板")
    private String service;

    // ServiceImpl模板
    @ApiModelProperty("ServiceImpl模板")
    private String serviceImpl;

    // Controller模板
    @ApiModelProperty("Controller模板")
    private String controller;
}
