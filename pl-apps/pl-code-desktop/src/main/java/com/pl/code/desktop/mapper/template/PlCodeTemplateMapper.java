package com.pl.code.desktop.mapper.template;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pl.code.desktop.entity.po.template.PlCodeTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClasssName PlCodeTemplateMapper
 * @Description 模板Mapper
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@Mapper
public interface PlCodeTemplateMapper extends BaseMapper<PlCodeTemplate> {
}
