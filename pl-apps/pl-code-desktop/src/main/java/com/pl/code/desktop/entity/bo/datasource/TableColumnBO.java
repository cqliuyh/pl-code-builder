package com.pl.code.desktop.entity.bo.datasource;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName TableColumnBO
 * @Description 表字段BO
 * @Author Liuyh
 * @Date 2021/9/23
 * @Version V0.0.1
 */
@ApiModel("表字段BO")
@Data
public class TableColumnBO {
    /**
     * 列表
     */
    @ApiModelProperty("列表")
    private String columnName;

    /**
     * 数据类型
     */
    @ApiModelProperty("数据类型")
    private String dataType;

    /**
     * 注释
     */
    @ApiModelProperty("注释")
    private String comments;

    /**
     * 其他信息
     */
    @ApiModelProperty("其他信息")
    private String extra;

    /**
     * 字段类型
     */
    @ApiModelProperty("字段类型")
    private String columnType;

    /**
     * 是否可以为空
     */
    @ApiModelProperty("是否可以为空")
    private Boolean nullable;

    /**
     * 键类型
     */
    @ApiModelProperty("键类型")
    private String columnKey;
}
