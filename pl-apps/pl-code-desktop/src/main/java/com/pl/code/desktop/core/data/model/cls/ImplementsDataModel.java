package com.pl.code.desktop.core.data.model.cls;

import com.pl.code.desktop.core.utils.DataModelUtil;
import lombok.Data;

/**
 * @ClasssName ImplementsDataModel
 * @Description 实现类数据模型
 * @Author Liuyh
 * @Date 2021/10/20
 * @Version V0.0.1
 */
@Data
public class ImplementsDataModel extends ExtendsClassDataModel{

    /**
     * 创建实现类数据模型
     *
     * @param packageName
     * @return
     */
    public static ImplementsDataModel build(String packageName) {
        // 通过包名过去类名
        String className = DataModelUtil.getClassNameByPackageName(packageName);
        return build(packageName, className);
    }

    /**
     * 创建实现类数据模型
     *
     * @param packageName
     * @param className
     * @return
     */
    public static ImplementsDataModel build(String packageName, String className) {
        ImplementsDataModel implementsDataModel = new ImplementsDataModel();
        implementsDataModel.setPackageName(packageName);
        implementsDataModel.setClassName(className);
        return implementsDataModel;
    }
}
