package com.pl.code.desktop.core.config.properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClasssName ControllerConfigProperties
 * @Description controller 配置属性
 * @Author Liuyh
 * @Date 2021/6/1
 * @Version V0.0.1
 */
@ApiModel("controller 配置属性")
@Data
public class ControllerProperties {
    // 是否生成基础API接口，如：page、list、getById、remove、save、update
    @ApiModelProperty("是否生成基础API接口")
    private boolean restfulApi;
    // 是否开启参数验证
    @ApiModelProperty("是否开启参数验证")
    private boolean validated;
    // 响应实体配置属性
    @ApiModelProperty("响应实体配置属性")
    private ResponseEntityProperties responseEntity;
}
