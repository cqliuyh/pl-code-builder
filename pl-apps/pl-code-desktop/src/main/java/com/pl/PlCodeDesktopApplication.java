package com.pl;

import com.pl.datasource.dynamic.annotation.EnableDynamicDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDynamicDataSource
@SpringBootApplication
public class PlCodeDesktopApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlCodeDesktopApplication.class, args);
    }

}
